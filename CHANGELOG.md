# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2022-03-24)


### Features

* **cart:** 購物車切版完成 ([e07de5c](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/e07de5c4af0b13731e436008baf8abef919c721f))
* **layout:** 修復地址搜尋功能 ([a8d70ae](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/a8d70ae49e4a3de23b820f3896e6834da9908c29))
* **order:** 新增訂單檢視查詢抓面 ([d393a61](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/d393a61d39a88eeb04467b9528231af47e69e371))
* **project:** 完成第一階段切版 ([3454494](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/3454494531c0fe58bbfaa17252234383b56098c6))
* **project:** 新增SSR、LayOut、首頁及定位地址查詢功能 ([bbe9211](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/bbe9211c4168fcb35320f5920608422bdb41a9fe))
* **project:** added universal package by angular-cli, made SSR success ([12f7f93](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/12f7f93a53bee36286772c179c03a1bc57d9c76a))
* **project:** fix wrong code ([f824c5a](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/f824c5a206d73353cb0c4fa571dc71ba419dff00))
* **project:** init ([306fe38](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/306fe386f866ce0ac3b21cfbd93e1e9018fd869c))
* **project:** moved files from old project,  then fixed error ([0ed53e5](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/0ed53e570d0c9f2ab8cb217e4cd084d74828e115))
* **project:** resolving sometion wrong with Angular and TypeScript with whole Project ([692160a](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/692160a17e7493f9d06926df1bc1c16ee038c133))
* **store & product:** 完成店家列表功能及商品完成店家列表功能及商品Menu瀏覽功能 ([ec23b6d](https://git.cloud-interactive.com/eu-money-team/jk/fe/commit/ec23b6d0dbcec083fb1d621d41925e25af0b3f85))
