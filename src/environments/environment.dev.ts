import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  target: 'dev',
  production: false,
  apiUrl: 'https://dev.justkitchen.com/',
  facebookAppId: '4234664876592523', // Lun Huang
  googlePlaceAutoComplete: 'AIzaSyCQQ5v8BuOCM33_YFiYFGSLpefu4a6jqLM',
  disableConsole: false,
  logLevel: NgxLoggerLevel.DEBUG,
  serverLogLevel: NgxLoggerLevel.OFF,
  serverLogUrl: 'https://www.googleapis.com/enable-above-if-needed/',
};
