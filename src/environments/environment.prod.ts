import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  target: 'prod',
  production: true,
  apiUrl: 'https://apidev.justkitchen.com/',
  facebookAppId: 'replace-with-client-id',
  googlePlaceAutoComplete: 'AIzaSyCQQ5v8BuOCM33_YFiYFGSLpefu4a6jqLM',
  disableConsole: true,
  logLevel: NgxLoggerLevel.OFF,
  serverLogLevel: NgxLoggerLevel.OFF,
  serverLogUrl: 'https://www.googleapis.com/enable-above-if-needed/',
};
