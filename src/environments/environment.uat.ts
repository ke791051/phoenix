import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  target: 'uat',
  production: false,
  apiUrl: 'http://localhost:3000/api/',
  facebookAppId: '4234664876592523', // Lun Huang
  googlePlaceAutoComplete: 'AIzaSyCQQ5v8BuOCM33_YFiYFGSLpefu4a6jqLM', // waylunly@gmail.com
  disableConsole: true,
  logLevel: NgxLoggerLevel.OFF,
  serverLogLevel: NgxLoggerLevel.OFF,
  serverLogUrl: 'https://www.googleapis.com/enable-above-if-needed/',
};
