// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  target: 'local',
  production: false,
  apiUrl: 'https://dev.justkitchen.com',
  facebookAppId: '4234664876592523',
  googlePlaceAutoComplete: 'AIzaSyCQQ5v8BuOCM33_YFiYFGSLpefu4a6jqLM', // waylunly@gmail.com
  disableConsole: false,
  logLevel: NgxLoggerLevel.DEBUG,
  serverLogLevel: NgxLoggerLevel.OFF,
  serverLogUrl: 'http://localhost:3000/api/logs/enable-above-if-needed/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
