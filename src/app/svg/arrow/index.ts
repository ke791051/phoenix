import { appDownIcon } from './down';
import { appLeftIcon } from './left';
import { appRightIcon } from './right';
import { appTopIcon } from './top';
export const arrowIcons = [appDownIcon, appLeftIcon, appRightIcon, appTopIcon];
