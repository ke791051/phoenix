export const appDownIcon = {
    data: `<svg width="12" height="8" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M.343 1.643L6 7.3l5.657-5.657L10.714.7 6 5.415 1.286.7l-.943.943z" fill="currentColor"/></svg>`,
    name: 'down'
};