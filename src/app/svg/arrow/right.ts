export const appRightIcon = {
    data: `<svg width="8" height="14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.37 5.946L1.71.296a1 1 0 10-1.42 1.41l4.95 5-4.95 4.95a1 1 0 000 1.41 1 1 0 00.71.3 1.003 1.003 0 00.71-.3l5.66-5.65a1 1 0 000-1.47z" fill="currentColor"/></svg>`,
    name: 'right'
};