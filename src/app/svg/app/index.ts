import { appAndroidIcon } from './android';
import { appIosIcon } from './ios';
export const appIcons = [appAndroidIcon, appIosIcon];
