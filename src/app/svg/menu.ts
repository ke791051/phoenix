export const appMenuIcon = {
    data: `<svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15 18H3v-2h12v2zm6-5H3v-2h18v2zm-6-5H3V6h12v2z" fill="#C9A063"/></svg>`,
    name: 'menu'
};