import { appItemBrand0Icon } from './item-brand-0';
import { appItemBrand1Icon } from './item-brand-1';
import { appItemBrand2Icon } from './item-brand-2';
import { appItemBrand3Icon } from './item-brand-3';
import { appItemBrand4Icon } from './item-brand-4';
import { appItemBrand5Icon } from './item-brand-5';
import { appItemBrand6Icon } from './item-brand-6';
import { appItemBrand7Icon } from './item-brand-7';
import { appItemBrand8Icon } from './item-brand-8';
import { appItemBrand9Icon } from './item-brand-9';
export const brandIcons = [
  appItemBrand0Icon,
  appItemBrand1Icon,
  appItemBrand2Icon,
  appItemBrand3Icon,
  appItemBrand4Icon,
  appItemBrand5Icon,
  appItemBrand6Icon,
  appItemBrand7Icon,
  appItemBrand8Icon,
  appItemBrand9Icon,
];
