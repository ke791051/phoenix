export const appGeoIcon = {
    data: `<svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M12 21a29.767 29.767 0 01-3.5-3.53C6.9 15.557 5 12.711 5 10a7 7 0 0111.952-4.95A6.955 6.955 0 0119 10c0 2.712-1.9 5.558-3.5 7.47A29.768 29.768 0 0112 21zm0-14a3 3 0 100 6 3 3 0 000-6z" fill="#fff"/></svg>`,
    name: 'geo'
};