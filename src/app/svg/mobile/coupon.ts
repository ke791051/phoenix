export const appCouponIcon = {
    data: `<svg width="20" height="20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M5.5 4a1.5 1.5 0 100 3 1.5 1.5 0 000-3zm13.62 4.71L10.71.29A.999.999 0 0010 0H1a1 1 0 00-1 1v9a1 1 0 00.29.71l8.42 8.41a3 3 0 004.24 0L19.12 13a3 3 0 000-4.24v-.05zm-1.41 2.82l-6.18 6.17a1 1 0 01-1.41 0L2 9.59V2h7.59l8.12 8.12a1.001 1.001 0 010 1.41z" fill="currentColor"/></svg>`,
    name: 'coupon'
};