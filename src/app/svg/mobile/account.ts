export const appAccountIcon = {
    data: `<svg width="20" height="21" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13.646 10.716a6 6 0 10-7.42 0 10 10 0 00-6.22 8.18 1.006 1.006 0 002 .22 8 8 0 0115.9 0 1 1 0 001 .89h.11a1 1 0 00.88-1.1 10 10 0 00-6.25-8.19zm-3.71-.71a4 4 0 110-8 4 4 0 010 8z" fill="currentColor"/></svg>`,
    name: 'account'
};