export const appOrderIcon = {
    data: `<svg width="20" height="21" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11 14.004H5a1 1 0 100 2h6a1 1 0 000-2zm-4-6h2a1 1 0 100-2H7a1 1 0 000 2zm12 2h-3v-9a1 1 0 00-1.5-.87l-3 1.72-3-1.72a1 1 0 00-1 0l-3 1.72-3-1.72a1 1 0 00-1.5.87v16a3 3 0 003 3h14a3 3 0 003-3v-6a1 1 0 00-1-1zm-16 8a1 1 0 01-1-1V2.734l2 1.14a1.08 1.08 0 001 0l3-1.72 3 1.72a1.08 1.08 0 001 0l2-1.14v14.27a3 3 0 00.18 1H3zm15-1a1 1 0 11-2 0v-5h2v5zm-7-7H5a1 1 0 100 2h6a1 1 0 000-2z" fill="currentColor"/></svg>`,
    name: 'order'
};