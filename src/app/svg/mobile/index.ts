import { appAccountIcon } from './account';
import { appCouponIcon } from './coupon';
import { appHomeIcon } from './home';
import { appOrderIcon } from './order';
export const mobileIcons = [appAccountIcon, appCouponIcon, appHomeIcon, appOrderIcon];
