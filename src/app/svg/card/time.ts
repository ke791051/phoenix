export const appTimeIcon = {
    data: `<svg width="13" height="12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M8.202 5.417H7.035V3.083a.583.583 0 10-1.166 0V6a.583.583 0 00.583.583h1.75a.583.583 0 100-1.166zM6.452.167a5.833 5.833 0 100 11.666 5.833 5.833 0 000-11.666zm0 10.5a4.666 4.666 0 110-9.333 4.666 4.666 0 010 9.333z" fill="currentColor"/></svg>`,
    name: 'time'
};