export const appPaperIcon = {
    data: `<svg width="24" height="24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18 3v0a3 3 0 013 3v2.143c0 .334 0 .501-.077.623a.5.5 0 01-.157.157C20.644 9 20.477 9 20.143 9H15m3-6v0a3 3 0 00-3 3v3m3-6H7c-1.886 0-2.828 0-3.414.586C3 4.172 3 5.114 3 7v14l3-1 3 1 3-1 3 1V9" stroke="#C9A063" stroke-width="2"/><path d="M7 7h4M8 11H7M7 15h3" stroke="#C9A063" stroke-width="2" stroke-linecap="round"/></svg>`,
    name: 'paper'
};