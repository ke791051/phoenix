import { appItemHot0Icon } from './item-hot-0';
import { appItemHot1Icon } from './item-hot-1';
import { appItemHot2Icon } from './item-hot-2';
import { appItemHot3Icon } from './item-hot-3';
import { appItemHot4Icon } from './item-hot-4';
import { appItemHot5Icon } from './item-hot-5';
import { appItemHot6Icon } from './item-hot-6';
import { appItemHot7Icon } from './item-hot-7';
export const hotIcons = [
  appItemHot0Icon,
  appItemHot1Icon,
  appItemHot2Icon,
  appItemHot3Icon,
  appItemHot4Icon,
  appItemHot5Icon,
  appItemHot6Icon,
  appItemHot7Icon,
];
