export const appFbIcon = {
    data: `<svg width="11" height="19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.976 10.824l.51-3.324h-3.19V5.344c0-.91.446-1.797 1.875-1.797h1.45V.717S9.305.492 8.047.492c-2.627 0-4.344 1.592-4.344 4.474V7.5H.783v3.324h2.92v8.036c1.19.187 2.403.187 3.594 0v-8.036h2.68z" fill="#fff"/></svg>`,
    name: 'fb'
};