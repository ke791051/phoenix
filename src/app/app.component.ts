import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AppService } from './core/services/app.service';

@UntilDestroy()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public isHome: boolean;
  public title: string;

  constructor(private router: Router, private appService: AppService) {
    this.isHome = false;
    this.title = 'JustKitchen';
  }

  ngOnInit(): void {
    this.router.events.pipe(untilDestroyed(this)).subscribe(val => {
      if (val instanceof NavigationEnd) {
        const path = val.urlAfterRedirects || '';
        const currentPage = path.split('/')[1] ? path.split('/')[1] : 'index';

        this.appService.handleCurrentPageChange(currentPage);
        this.isHome = /^\/home$/.test(path);
        this.appService.handleCurrentPageChange(currentPage);
      }
    });
  }
}
