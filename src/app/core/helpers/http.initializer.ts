import { HttpClient } from '@angular/common/http';
import { HttpMockService } from '@app/core/services/http/http-mock.service';
import { environment } from '../../../environments/environment';
import { IHttpService } from '../interface/IHttpService';
import { HttpClientService } from '../services/http/http-client.service';

export const httpServiceFactory = (http: HttpClient): IHttpService => {
  if (environment.production) {
    return new HttpClientService(http);
  }
  return new HttpClientService(http);
};
