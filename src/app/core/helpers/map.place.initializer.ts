import { GoogleMapService } from './../services/google-map.service';
/* eslint-disable prefer-const */
import { environment } from 'src/environments/environment';
import { google, Loader, LoaderOptions } from 'google-maps';
import { TranslocoService } from '@ngneat/transloco';

export function mapPlaceLoader(trans: TranslocoService, gms: GoogleMapService) {
  const options: LoaderOptions = {
    libraries: ['places', 'geometry'],
    language: trans.getActiveLang(),
  };
  const loader = new Loader(environment.googlePlaceAutoComplete, options);
  return () =>
    new Promise(async (resolve: any, reject: any) => {
      try {
        const google: google = await loader.load();
        gms.handleGoogleUpdate(google);
        resolve();
      } catch (e) {
        reject(e);
      }
    });
}
