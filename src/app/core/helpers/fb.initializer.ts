/* eslint-disable prefer-const */
import { environment } from 'src/environments/environment';
import { get, set } from 'lodash';
import { AuthService } from '../services/auth.service';

export function fbInitializer(authService: AuthService) {
  return () =>
    new Promise((resolve: any) => {
      // wait for facebook sdk to initialize before starting the angular app
      window['fbAsyncInit'] = function () {
        try {
          FB.init({
            appId: environment.facebookAppId,
            cookie: true,
            xfbml: true,
            version: 'v11.0',
          });

          // auto authenticate with the api if already logged in with facebook
          FB.getLoginStatus(({ authResponse }) => {
            if (authResponse) {
              console.log('FB auth response', authResponse);
              authService.apiAuthenticate(authResponse.accessToken).subscribe().add(resolve);
            } else {
              resolve();
            }
          });
        } catch (e: unknown) {
          resolve(e);
        }
      };

      // load facebook sdk script
      (function (d, s, id) {
        let js: HTMLElement;
        const tmp = d.getElementsByTagName(s);
        const fjs = get(tmp, '0') || new HTMLElement();
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        set(js, 'src', 'https://connect.facebook.net/en_US/sdk.js');
        fjs.parentNode.insertBefore(js, fjs);
      })(document, 'script', 'facebook-jssdk');
    });
}
