export interface SVGITEMS {
  key: string
  txt: string
  redirectTo: string
}

export interface IMGITEMS {
  img: string
  redirectTo?: string
}
