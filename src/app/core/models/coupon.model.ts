export type coupon = {
  customer_coupons_id: number;
  coupon_name: string;
  coupon_id: string;
  customer_no: string;
  used: number;
  order_no: string;
  coupon_desc: string;
  vendor_id: string;
  is_open: number;
  isOpen: boolean;
  start_date_at: Date;
  end_date_at: Date;
  coupon_mode: number;
  discount_mode: number;
  sale_off: number;
  discount_amount: number;
  product_id: string;
  usage_count: number;
  quota: number;
  need_code: number;
  code: string;
};

export type ReceiveCoupon = {
  customer_no: string;
  coupon_id: string;
  is_open: number;
};
