import { Product } from '../classes/Product';
export type ProductModel = {
  store_id: string;
  brand_id: string;
  brand_name: string;
  product_id: string;
  product_name: string;
  product_name2: string;
  product_pic: string;
  product_desc: string;
  price: number;
  special_price: number;
  calorie: number;
  popular: number;
  options: Array<Option>;
  promotion_rule: PromotionRule;
};

export type BrandProductModel = {
  brandName: string;
  list: ProductModel[];
};

export type DirtyProduct = {
  store_id?: string;
  brand_id?: string;
  brand_name?: string;
  product_id?: string;
  product_name?: string;
  product_name2?: string;
  product_pic?: string;
  product_desc?: string;
  price?: number;
  special_price?: number;
  calorie?: number;
  popular?: number;
  options?: Array<Option>;
  promotion_rule?: PromotionRule;
};

export type Option = {
  id: string;
  option_name: string;
  default_option: number;
  necessary: number;
  isNecessary?: boolean;
  multiple_select: number;
  isMultipleSelect?: boolean;
  max_select: number;
  option_desc: string;
  option_items: Array<OptionItems>;
};

export type OptionItems = {
  product_id: string;
  product_name: string;
  product_name2: string;
  product_pic: string;
  product_desc: string;
  price: number;
  special_price: number;
};

export type PromotionRule = {
  promotion_mode: string;
  sale_off: number;
  discount_amount: number;
  promotion_name: string;
  promotion_desc: string;
  start_date_at?: Date;
  end_date_at?: Date;
};

export type BasicOrderModel = {
  price: number;
  count: number;
  remark: string;
};

export type CartProductModel = BasicOrderModel & {
  product: ProductModel;
  plus_item: PlusItemModel[];
};

export type PlusItemModel = BasicOrderModel & {
  product_id: string;
  product: OptionItems;
};
