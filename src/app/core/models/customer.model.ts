export type CustomerModel = {
  customer_no: string;
  current_amount: string;
  name: string;
  cellphone: string;
  email: string;
  auth: string;
  birth: Date;
  status: string;
  bonus_point: string;
  google_account: string;
  fb_account: string;
  send_mail_time: string;
  created_at: Date;
  updated_at: Date;
};
