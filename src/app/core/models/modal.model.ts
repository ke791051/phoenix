import { IModalContentComponent } from '../interface/IModalContentComponent';
export type ModalOptionModel = {
  modalClass?: string;
  closeBtn?: boolean;
  backdropClass?: string;
};

export type ModalVisibleModel = {
  visible: boolean;
  content?: IModalContentComponent;
  instances?: any;
};

// export type ModalFooterModel = {
//   //
// }
