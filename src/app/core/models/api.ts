/* Use replace {variable} later in provider/service */

export class Api {
  /* Vendor API */
  getVendorAll = '/vendor/getall';
  postVendorUpdate = '/vendor/update';
  postVendorAppend = '/vendor/append';
  postVendorRemove = '/vendor/remove';
  postVendorGetById = '/vendor/getbyid';

  /* Brands API */
  getBrandsAll = '/brands/getall';
  postBrandsUpdate = '/brands/update';
  postBrandsAppend = '/brands/append';
  postBrandsRemove = '/brands/remove';
  postBrandsGetByVendor = '/brands/getbyvendor';
  postBrandsGetById = '/brands/getbyid';
  postBrandsUpdatePromotion = '/brands/updatepromotion';

  /* Products API */
  getProductsAll = '/products/getall';
  postProductsUpdate = '/products/update';
  postProductsAppend = '/products/append';
  postProductsRemove = '/products/remove';
  postProductsGetByBrand = '/products/getbybrand';
  postProductsGetById = '/products/getbyid';
  postProductsGetByVendor = '/products/getbyvendor';

  /* Stores API */
  getStoresAll = '/stores/getall';
  postStoresUpdate = '/stores/update';
  postStoresAppend = '/stores/append';
  postStoresRemove = '/stores/remove';
  postStoresGetById = '/stores/getbyid';

  /* Store Products API */
  getStoreProductsAll = '/store-products/getall';
  postStoreProductsUpdate = '/store-products/update';
  postStoreProductsAppend = '/store-products/append';
  postStoreProductsRemove = '/store-products/remove';
  postStoreProductsGetByStore = '/store-products/getbystore';
  postStoreProductsGetById = '/store-products/getbyid';

  /* Promotion API */
  getPromotionAll = '/promotion/getall';
  postPromotionUpdate = '/promotion/update';
  postPromotionAppend = '/promotion/append';
  postPromotionRemove = '/promotion/remove';
  postPromotionGetById = '/promotion/getbyid';

  /* Coupo API */
  getCouponAll = '/coupon/getall';
  postCouponUpdate = '/coupon/update';
  postCouponAppend = '/coupon/append';
  postCouponRemove = '/coupon/remove';

  /* Order API */
  postOrder = '/order';
  getOrder = '/order';
  getOrderWithNo = '/order/{order_no}';
  patchOrderWithNo = '/order/{order_no}';
  patchOrderWithNoCancel = '/order/{order_no}/cancel';
  postPayment = '/payment';

  /* Account API */
  postCustomer = '/customer';
  patchCustomerWithNo = '/customer/{customer_no}';
  getCustomerWithNo = '/customer/{customer_no}';
  getCustomer = '/customer';
  postAccount = '/account';
  patchAccount = '/account/{account}';
  getAccount = '/account';
}

export type ApiResponse<T> = {
  status: boolean;
  result: T;
  message: string;
};
