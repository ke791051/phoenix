import { Week } from './store.model';

export type PopularModel = {
  brand_id: string;
  brand_name: string;
  brand_pic: string;
  brand_desc: string;
  vendor_id: string;
  tags: string;
  popular: number;
  isPopular: boolean;
  week: Array<Week>;
};

export type DirtyPopular = {
  brand_id?: string;
  brand_name?: string;
  brand_pic?: string;
  brand_desc?: string;
  vendor_id?: string;
  tags?: string;
  popular?: number;
  week?: Array<Week>;
};
