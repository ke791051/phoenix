export type GoogleLatLngBounds = {
  north: number;
  south: number;
  east: number;
  west: number;
};

export type addressComponentModel = {
  long_name: string;
  short_name: string;
  types: Array<string>;
};

export interface IAddressComponent {
  longName: string;
  shortName: string;
  types: Array<string>;
}
