import { dataType } from '../../types/data.type';

export interface RxBusModel {
  cmd: string;
  data: dataType;
}
