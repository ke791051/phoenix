export type EventMessageModel<T> = {
  status: boolean;
  msg: string;
  result?: T;
};

export type ErrorMsgEvent = {
  msg: string;
  func: () => void;
};
