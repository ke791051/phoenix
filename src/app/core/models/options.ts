import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Subject } from 'rxjs';
import { dataType } from '../../types/data.type';
export interface HttpDefaultOptions {
  baseApiURL: string;
  headers: dataType;
}
export interface BaseJWTOptions extends BaseOption {
  key: string;
}
export interface BaseOption {
  isInit: boolean;
}

export declare class LoggerOptions {
  enableServerLog?: boolean;
  serverURL?: string;
  disableConsoleLog?: boolean;
}
export interface HttpOption {
  headers?: HttpHeaders | { [header: string]: string | string[] };
  observe?: 'body' | 'events' | 'response';
  params?: HttpParams | { [param: string]: string | number | boolean | ReadonlyArray<string | number | boolean> };
  reportProgress?: boolean;
  responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
  withCredentials?: boolean;
}
