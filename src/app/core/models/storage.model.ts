export type StorageEventModel = { type: StorageType; key: string; str: string };

export enum StorageType {
  local = 'localStorage',
  session = 'sessionStorage',
}
