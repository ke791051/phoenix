export interface SLIDES {
  img: string
  title: string
  redirectTo: string
}
