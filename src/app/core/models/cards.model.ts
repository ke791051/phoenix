import { BehaviorSubject } from 'rxjs';

export class PRODUCT {
  public type: string;
  public title: string;
  public card: CardModel[];

  constructor() {
    this.type = '';
    this.title = '';
    this.card = [];
  }
}

export interface CardModel {
  id?: string;
  img: string;
  promo?: string;
  title: string;
  locate: string;
  time: string;
  redirectTo: string;
  promotionMode: number;
}

export type CardGroup = Array<CardModel[]>;
