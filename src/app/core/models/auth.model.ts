export type AuthModel = {
  id: string;
  facebookId: string;
  name: string;
  extraInfo: string;
  token?: string;
};

export type DirtyAuth = {
  id?: string;
  facebookId?: string;
  name?: string;
  extraInfo?: string;
  token?: string;
};
