import { dataType } from 'src/app/types/data.type';
import { HttpReq as IHttpReq } from '../interface/IHttpService';

export class HttpReq implements IHttpReq {
  public headers?: dataType;
  public queryObject?: dataType;
  public body?: dataType;
  public baseURL?: string;

  constructor(req?: Partial<HttpReq>) {
    if (!req) return;
    Object.assign(this, req);
  }
}
