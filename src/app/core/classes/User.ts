import { get } from 'lodash';
import { CustomerAddress, CustomerModel, IUser } from '../models/user.modal';
import { getString } from '../utils/lodash.function';

export class User implements CustomerModel {
  public customer_no: string;
  public current_amount: string;
  public name: string;
  public cellphone: string;
  public email: string;
  public auth: string;
  public birth: Date;
  public status: string;
  public bonus_point: string;
  public google_account: string;
  public fb_account: string;
  public send_mail_time: string;
  public created_at: Date;
  public updated_at: Date;
  public customer_address: Array<CustomerAddress>;

  constructor(payload?: unknown) {
    console.log(payload);
    this.customer_no = getString(payload, 'customer_no', '');
    this.current_amount = getString(payload, 'current_amount', '');
    this.name = getString(payload, 'name', '');
    this.cellphone = getString(payload, 'cellphone', '');
    this.email = getString(payload, 'email', '');
    this.auth = getString(payload, 'auth', '');
    this.birth = get(payload, 'birth', new Date());
    this.status = getString(payload, 'status', '');
    this.bonus_point = getString(payload, 'bonus_point', '');
    this.google_account = getString(payload, 'google_account', '');
    this.fb_account = getString(payload, 'fb_account', '');
    this.send_mail_time = getString(payload, 'send_mail_time', '');
    this.created_at = get(payload, 'created_at', new Date());
    this.updated_at = get(payload, 'updated_at', new Date());
    this.customer_address = get(payload, 'customer_address', []);
  }
}
