import { AuthModel, DirtyAuth } from '../models/auth.model';
import { getString } from '../utils/lodash.function';
export class Auth implements AuthModel {
  public id: string;
  public facebookId: string;
  public name: string;
  public extraInfo: string;
  public token?: string;

  constructor(payload?: DirtyAuth) {
    this.id = getString(payload, 'id', '');
    this.facebookId = getString(payload, 'facebookId', '');
    this.name = getString(payload, 'name', '');
    this.extraInfo = getString(payload, 'extraInfo', '');
    this.token = getString(payload, 'token', '');
  }
}
