import { BaseJWTOptions, BaseOption } from 'src/app/core/models/options';
import { dataType } from 'src/app/types/data.type';
export class JWTOptions implements BaseJWTOptions {
  public isInit: boolean;
  public key: string;

  constructor(newKey: string) {
    this.isInit = false;
    this.key = newKey;
  }
}

export class BaseHttpConfig implements BaseOption {
  public isInit: boolean;
  public baseApiURL: string;
  public headers: dataType;

  constructor(newBaseApiURL: string, newHeader: dataType) {
    this.isInit = false;
    this.baseApiURL = newBaseApiURL;
    this.headers = newHeader;
  }
}
