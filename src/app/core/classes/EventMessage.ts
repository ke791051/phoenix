import { EventMessageModel } from '../models/event-message.model';

export class EventMessage<T> implements EventMessageModel<T> {
  public status: boolean;
  public msg: string;
  public result?: T;
  constructor(status?: boolean, msg?: string, res?: T) {
    this.status = status || true;
    this.msg = msg || '';
    this.result = res;
  }
}
