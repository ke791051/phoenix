import { get } from 'lodash';
import { PopularModel, DirtyPopular } from '../models/popular.model';
import { Week } from '../models/store.model';

export class Popular implements PopularModel {
  public brand_id: string;
  public brand_name: string;
  public brand_pic: string;
  public brand_desc: string;
  public vendor_id: string;
  public tags: string;
  public popular: number;
  public week: Array<Week>;
  public isPopular: boolean;

  constructor(data: DirtyPopular) {
    this.brand_id = get(data, 'brand_id', '');
    this.brand_name = get(data, 'brand_name', '');
    this.brand_pic = get(data, 'brand_pic', '');
    this.brand_desc = get(data, 'brand_desc', '');
    this.vendor_id = get(data, 'vendor_id', '');
    this.tags = get(data, 'tags', '');
    this.popular = get(data, 'popular', 0);
    this.week = get(data, 'week', []);
    this.isPopular = this.popular === 1;
  }
}
