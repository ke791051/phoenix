import {
  DirtyOrderItemObject,
  OrderCoupon,
  OrderItemList,
  OrderPayloadModel,
  OrderResponseDirtyModel,
  OrderResponseModel,
  Payment,
  PlusOrderDirtyItem,
  PlusOrderItem,
  PreOrderModel,
  PreOrderPlusItemModel,
  Shipment,
} from '../models/order.model';
import { CartProductModel, PlusItemModel } from '../models/product.model';
import { getNumber, getString } from '../utils/lodash.function';
import { StoreModel } from '../models/store.model';
import { CustomerModel } from '../models/user.modal';
import { get } from 'lodash';
import { OrderDirtyItemList, DirtyShipment } from '../models/order.model';
export class PreOrderItem implements PreOrderModel {
  public product_id: string;
  public price: number;
  public count: number;
  public remark: string;
  public plus_item: PreOrderPlusItemModel[];

  constructor(payload?: CartProductModel) {
    this.product_id = getString(payload, 'product_id', '');
    this.price = getNumber(payload, 'price', 0);
    this.count = getNumber(payload, 'count', 0);
    this.remark = getString(payload, 'remark', '');
    this.plus_item = this.plusItemParser(payload);
  }

  private plusItemParser(payload?: CartProductModel): PreOrderPlusItemModel[] {
    if (!payload) return [];
    const list: PlusItemModel[] = payload.plus_item;
    const res: PreOrderPlusItemModel[] = list.map((item: PlusItemModel) => {
      const tmp: PreOrderPlusItemModel = {
        product_id: item.product_id,
        price: item.price,
        count: item.count,
        remark: item.remark,
      };
      return tmp;
    });
    return res;
  }
}

export class OrderPayload implements OrderPayloadModel {
  public origin_city: string;
  public origin_area: string;
  public origin_address: string;
  public origin_zip_code: string;
  public dest_zip_code: string;
  public store_id: string;
  public item_list: Array<PreOrderModel>;
  public customer_no: string;
  public dest_name: string;
  public dest_phone: string;
  public email: string;
  public payment_way: string;
  public shipment_way: string;
  public expected_get_at: string;
  public expected_send_at: string;
  public remark: string;
  public token: string;

  constructor(payload?: any) {
    if (!payload) {
      this.token = 'n9x5qn8stp68gnvau42qrzmyuvxb4ytr';
      this.remark = 'Dino測試單';
      this.payment_way = '5';
      this.shipment_way = '0';
      this.store_id = 'TWNe01';
      this.origin_city = '台北市';
      this.origin_area = '內湖區';
      this.origin_address = '陽光街365巷39號1樓';
      this.origin_zip_code = '114';
      this.customer_no = '';
      // this.dest_city='台北市';
      // this.dest_area='內湖區';
      // this.dest_address='瑞湖街178巷15號';
      this.dest_zip_code = '114';
      this.dest_name = 'Dino';
      this.dest_phone = '0905552611';
      this.email = 'dinozzo.su@justkitchen.com';
      this.item_list = [
        {
          product_id: 'jk108obg201',
          price: 350,
          count: 1,
          remark: '',
          plus_item: [],
        },
      ];
      this.expected_get_at = '2022-03-08 09:30:46';
      this.expected_send_at = '2022-03-08 09:30:46';
      return;
    }

    this.origin_city = '';
    this.origin_area = '';
    this.origin_address = '';
    this.origin_zip_code = '';
    this.dest_zip_code = '';
    this.store_id = '';
    this.item_list = [];
    this.customer_no = '';
    this.dest_name = '';
    this.dest_phone = '';
    this.email = '';
    this.payment_way = '';
    this.shipment_way = '';
    this.expected_get_at = '';
    this.expected_send_at = '';
    this.remark = '';
    this.token = '';
  }

  private plusItemParser(payload?: CartProductModel): PreOrderPlusItemModel[] {
    if (!payload) return [];
    const list: PlusItemModel[] = payload.plus_item;
    const res: PreOrderPlusItemModel[] = list.map((item: PlusItemModel) => {
      const tmp: PreOrderPlusItemModel = {
        product_id: item.product_id,
        price: item.price,
        count: item.count,
        remark: item.remark,
      };
      return tmp;
    });
    return res;
  }
}

export class OrderResponse implements OrderResponseModel {
  public order_no: string;
  public pickup_no: string;
  public state: string;
  public payment_way: string;
  public payment_state: string;
  public shipment_way: string;
  public shipment_state: string;
  public customer_name: string;
  public customer_no: string;
  public customer_id: string;
  public sub_total_price: number;
  public discount_store: number;
  public discount_brand: number;
  public discount_coupon: number;
  public discount_amount: number;
  public shipping: number;
  public paid_shipping: number;
  public payment_amount: number;
  public pos_order_no: string;
  public pos_status: number;
  public origin_city: string;
  public origin_area: string;
  public origin_address: string;
  public origin_zip_code: string;
  public dest_city: string;
  public dest_area: string;
  public dest_address: string;
  public dest_zip_code: string;
  public expected_get_at: string;
  public expected_send_at: string;
  public dest_name: string;
  public dest_phone: string;
  public email: string;
  public store_id: string;
  public vat: string;
  public remark: string;
  public shipment_remark: string;
  public created_at: string;
  public updated_at: string;
  public created_user: string;
  private _item_list: Array<OrderItemList>;
  public item_list: Array<OrderItemList>;
  public payment: Array<Payment>;
  public shipment: Array<Shipment>;
  public order_coupon: Array<OrderCoupon>;

  constructor(res?: OrderResponseDirtyModel) {
    const _today: Date = new Date();
    const dirtyItemList: Array<OrderDirtyItemList> | DirtyOrderItemObject | undefined = get(res, 'item_list');
    let _orderItemList: Array<OrderItemList> = [];
    if (dirtyItemList) {
      if (Array.isArray(dirtyItemList)) {
        _orderItemList = dirtyItemList.map((item: OrderDirtyItemList) => {
          const _tmpPlus: PlusOrderDirtyItem[] = get(item, 'plus_item', []);
          const _newPlus: PlusOrderItem[] = _tmpPlus.map((tmp: PlusOrderDirtyItem) => {
            const _plus: PlusOrderItem = {
              id: getString(tmp, 'id', ''),
              order_no: getString(tmp, 'order_no', ''),
              product_id: getString(tmp, 'product_id', ''),
              product_name: getString(tmp, 'product_name', ''),
              price: getNumber(tmp, 'price', 0),
              count: getNumber(tmp, 'count', 0),
              total_price: getNumber(tmp, 'total_price', 0),
              remark: getString(tmp, 'remark', ''),
              plus_item_id: getString(tmp, 'plus_item_id', ''),
              created_at: getString(tmp, 'created_at', ''),
              updated_at: getString(tmp, 'updated_at', ''),
              product_name2: getString(tmp, 'product_name2', ''),
            };
            return _plus;
          });
          const _newItem: OrderItemList = {
            id: getString(item, 'id', ''),
            order_no: getString(item, 'order_no', ''),
            product_id: getString(item, 'product_id', ''),
            product_name: getString(item, 'product_name', ''),
            price: getNumber(item, 'price', 0),
            count: getNumber(item, 'count', 0),
            total_price: getNumber(item, 'total_price', 0),
            remark: getString(item, 'remark', ''),
            plus_item_id: getString(item, 'plus_item_id', ''),
            plus_item: _newPlus,
            created_at: getString(item, 'created_at', ''),
            updated_at: getString(item, 'updated_at', ''),
            product_name2: getString(item, 'product_name2', ''),
          };
          return _newItem;
        });
      }
    }
    const dirtyShipment: DirtyShipment[] = get(res, 'shipment', []);
    const shipment: Shipment[] = dirtyShipment.map((dirty: DirtyShipment) => {
      const newItem: Shipment = {
        id: getNumber(dirty, 'id', 0),
        order_no: getString(dirty, 'order_no', ''),
        final_fee: getNumber(dirty, 'final_fee', 0),
        pricing_method: getString(dirty, 'order_no', ''),
        distance_pricing: getString(dirty, 'order_no', ''),
        store_pricing_rule: getString(dirty, 'order_no', ''),
        fee: getNumber(dirty, 'fee', 0),
        remote_road: getString(dirty, 'remote_road', ''),
        promo_add_price: getNumber(dirty, 'promo_add_price', 0),
        excess_discount_price: getNumber(dirty, 'excess_discount_price', 0),
        exceed_distance_price: getNumber(dirty, 'exceed_distance_price', 0),
        exceed_distance: getNumber(dirty, 'exceed_distance', 0),
        distance: getNumber(dirty, 'distance', 0),
        discount: getNumber(dirty, 'discount', 0),
        exceed_meals_multiple: getNumber(dirty, 'exceed_meals_multiple', 0),
        meals_price: getNumber(dirty, 'meals_price', 0),
        base_meal_expenses: getNumber(dirty, 'base_meal_expenses', 0),
        exceed_weight_multiple: getNumber(dirty, 'exceed_weight_multiple', 0),
        weight: getNumber(dirty, 'weight', 0),
        base_weight: getNumber(dirty, 'base_weight', 0),
        unit_distance_price: getNumber(dirty, 'unit_distance_price', 0),
        unit_distance: getNumber(dirty, 'unit_distance', 0),
        base_price: getNumber(dirty, 'base_price', 0),
        base_distance: getNumber(dirty, 'base_distance', 0),
        origins: getString(dirty, 'origins', ''),
        destinations: getString(dirty, 'destinations', ''),
        customer_code: getString(dirty, 'customer_code', ''),
        quotation_id: getString(dirty, 'quotation_id', ''),
        request_id: getString(dirty, 'request_id', ''),
        remark: getString(dirty, 'remark', ''),
        status: getString(dirty, 'status', ''),
        shipping_id: getString(dirty, 'shipping_id', ''),
        addition_info: getString(dirty, 'addition_info', ''),
        request_status: getString(dirty, 'request_status', ''),
        delivered_at: getString(dirty, 'delivered_at', ''),
        total_freight: getNumber(dirty, 'total_freight', 0),
        meals_total: getNumber(dirty, 'meals_total', 0),
        enable: getString(dirty, 'enable', ''),
        type_code: getString(dirty, 'type_code', ''),
        type: getString(dirty, 'type', ''),
        created_at: getString(dirty, 'created_at', ''),
        updated_at: getString(dirty, 'updated_at', ''),
      };
      return newItem;
    });
    this.order_no = getString(res, 'order_no', '');
    this.pickup_no = getString(res, 'pickup_no', '');
    this.state = getString(res, 'state', '');
    this.payment_way = getString(res, 'payment_way', '');
    this.payment_state = getString(res, 'payment_state', '');
    this.shipment_way = getString(res, 'shipment_way', '');
    this.shipment_state = getString(res, 'shipment_state', '');
    this.customer_name = getString(res, 'customer_name', '');
    this.customer_no = getString(res, 'customer_no', '');
    this.customer_id = getString(res, 'customer_id', '');
    this.sub_total_price = getNumber(res, 'sub_total_price', 0);
    this.discount_store = getNumber(res, 'discount_store', 0);
    this.discount_brand = getNumber(res, 'discount_brand', 0);
    this.discount_coupon = getNumber(res, 'discount_coupon', 0);
    this.discount_amount = getNumber(res, 'discount_amount', 0);
    this.shipping = getNumber(res, 'shipping', 0);
    this.paid_shipping = getNumber(res, 'paid_shipping', 0);
    this.payment_amount = getNumber(res, 'payment_amount', 0);
    this.pos_order_no = getString(res, 'pos_order_no', '');
    this.pos_status = getNumber(res, 'sub_total_price', 0);
    this.origin_city = getString(res, 'origin_city', '');
    this.origin_area = getString(res, 'origin_area', '');
    this.origin_address = getString(res, 'origin_address', '');
    this.origin_zip_code = getString(res, 'origin_zip_code', '');
    this.dest_city = getString(res, 'dest_city', '');
    this.dest_area = getString(res, 'dest_area', '');
    this.dest_address = getString(res, 'dest_address', '');
    this.dest_zip_code = getString(res, 'dest_zip_code', '');
    this.expected_get_at = get(res, 'expected_get_at', _today.toDateString());
    this.expected_send_at = get(res, 'expected_send_at', _today.toDateString());
    this.dest_name = getString(res, 'dest_name', '');
    this.dest_phone = getString(res, 'dest_phone', '');
    this.email = getString(res, 'email', '');
    this.store_id = getString(res, 'store_id', '');
    this.vat = getString(res, 'vat', '');
    this.remark = getString(res, 'remark', '');
    this.shipment_remark = getString(res, 'shipment_remark', '');
    this.created_at = get(res, 'created_at', _today.toDateString());
    this.updated_at = get(res, 'updated_at', _today.toDateString());
    this.created_user = getString(res, 'created_user', '');
    this._item_list = _orderItemList;
    this.payment = get(res, 'payment', []);
    this.shipment = shipment;
    this.order_coupon = get(res, 'order_coupon', []);
    this.item_list = [];
    _orderItemList.forEach((item: OrderItemList) => {
      if (item.plus_item_id !== '') {
        const target: number = _orderItemList.findIndex((tmp: OrderItemList) => tmp.product_id === item.plus_item_id);
        if (target > -1) {
          _orderItemList[target].plus_item?.push(item);
        }
      }
    });
    this.item_list = _orderItemList;
  }
}
