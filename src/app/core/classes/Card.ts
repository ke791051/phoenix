import { CardModel } from '../models/cards.model';

export class Card implements CardModel {
  public id?: string;
  public img: string;
  public promotionMode: number;
  public promo?: string;
  public title: string;
  public locate: string;
  public time: string;
  public redirectTo: string;

  constructor(
    id: string = '',
    img: string,
    promotionMode: number = 0,
    promo: string = '',
    title: string,
    locate: string,
    time: string,
    redirectTo: string,
  ) {
    this.id = id;
    this.img = img;
    this.promotionMode = promotionMode;
    this.promo = promo;

    this.title = title;
    this.locate = locate;
    this.time = time;
    this.redirectTo = redirectTo;
  }
}
