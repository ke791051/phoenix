import { get } from 'lodash';
import { CartProductModel, PlusItemModel, ProductModel } from 'src/app/core/models/product.model';
import { getNumber, getString } from '../utils/lodash.function';
import { Product } from './Product';

export class Cart implements CartProductModel {
  public count: number;
  public remark: string;
  public price: number;
  public product: ProductModel;
  public plus_item: PlusItemModel[];
  public get product_id(): string {
    return this.product.product_id;
  }

  constructor(payload?: unknown) {
    this.count = getNumber(payload, 'count', 0);
    this.remark = getString(payload, 'remark', '');
    this.price = getNumber(payload, 'price', 0);
    this.product = get(payload, 'product', new Product());
    this.plus_item = get(payload, 'plus_item', []);
  }
}
