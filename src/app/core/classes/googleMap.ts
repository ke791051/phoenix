import { get } from 'lodash';
import { addressComponentModel, IAddressComponent } from '../models/google.model';
import { getString } from '../utils/lodash.function';

export class AddressComponent implements IAddressComponent {
  public longName: string;
  public shortName: string;
  public types: Array<string>;
  constructor(payLoad: addressComponentModel) {
    this.longName = getString(payLoad, 'long_name', '');
    this.shortName = getString(payLoad, 'short_name', '');
    this.types = get(payLoad, 'types', []) || [];
  }
}
