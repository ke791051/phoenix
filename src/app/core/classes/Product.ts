import { DirtyProduct, PlusItemModel, ProductModel, PromotionRule } from 'src/app/core/models/product.model';
import { get } from 'lodash';
import { Option, CartProductModel } from '../models/product.model';
import { getString, getNumber } from '../utils/lodash.function';

export class Product implements ProductModel {
  public store_id: string;
  public brand_id: string;
  public brand_name: string;
  public product_id: string;
  public product_name: string;
  public product_name2: string;
  public product_pic: string;
  public product_desc: string;
  public price: number;
  public special_price: number;
  public calorie: number;
  public popular: number;
  public options: Array<Option>;
  public promotion_rule: PromotionRule;

  constructor(data?: DirtyProduct) {
    const currentOptions: Option[] = get(data, 'options', []) || [];
    const _option: Array<Option> = currentOptions.map((opt: Option) =>
      Object.assign(opt, {
        isNecessary: opt.necessary === 1,
        isMultipleSelect: opt.multiple_select === 1,
      }),
    );
    this.store_id = get(data, 'store_id', '');
    this.brand_id = get(data, 'brand_id', '');
    this.brand_name = get(data, 'brand_name', '');
    this.product_id = get(data, 'product_id', '');
    this.product_name = get(data, 'product_name', '');
    this.product_name2 = get(data, 'product_name2', '');
    this.product_pic = get(data, 'product_pic', '');
    this.product_desc = get(data, 'product_desc', '');
    this.price = get(data, 'price', 0);
    this.special_price = get(data, 'special_price', 0);
    this.calorie = get(data, 'calorie', 0);
    this.popular = get(data, 'popular', 0);
    this.options = _option;
    this.promotion_rule = get(data, 'promotion_rule', {
      promotion_mode: '',
      sale_off: 0,
      discount_amount: 0,
      promotion_name: '',
      promotion_desc: '',
    });
  }
}

export class CartProduct implements CartProductModel {
  public product: ProductModel;
  public price: number;
  public count: number;
  public remark: string;
  public plus_item: PlusItemModel[];

  constructor(payload?: { product_id: string; price: number; count: number; remark?: string }) {
    this.product = get(payload, 'product', new Product());
    this.price = getNumber(payload, 'price', 0);
    this.count = getNumber(payload, 'count', 0);
    this.remark = getString(payload, 'remark', '');
    this.plus_item = [];
  }

  public addItems(payload: PlusItemModel | PlusItemModel[]): void {
    const tmp: PlusItemModel[] = Array.isArray(payload) ? payload : [payload];
    tmp.forEach((item: PlusItemModel) => {
      if (this.plus_item.findIndex((res: PlusItemModel) => res.product_id === item.product_id) === -1) {
        this.plus_item.push(item);
      }
    });
  }

  public remove(payload: PlusItemModel): void {
    const target: number = this.plus_item.findIndex((res: PlusItemModel) => res.product_id === payload.product_id);
    if (target > -1) {
      this.plus_item.splice(target, 1);
    }
  }
}
