import { get, has, isString } from 'lodash';
import { getString } from '../utils/lodash.function';
import { StorageType, StorageEventModel } from './../models/storage.model';
import { dataObject } from '@app/types/data.type';
export class WebStorage {
  private _db: dataObject;

  constructor() {
    this._db = {};
  }

  public setItem(key: string, val: string): boolean {
    if (key === '' || !isString(key) || !isString(val)) return false;
    this._db[key] = val;
    return true;
  }

  public getItem(key: string): string {
    if (key === '' || !isString(key) || has(this._db, key)) return '';
    return getString(this._db, key, '');
  }

  public clear(): void {
    this._db = {};
  }
}

export class WebStorageModel implements StorageEventModel {
  public type: StorageType;
  public key: string;
  public str: string;

  constructor(payload?: any) {
    this.type = get(payload, 'type', StorageType.local);
    this.key = getString(payload, 'key', '');
    this.str = getString(payload, 'str', '');
  }
}
