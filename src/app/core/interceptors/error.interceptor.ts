import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { NGXLogger } from 'ngx-logger'
import { Observable, throwError } from 'rxjs'
import { catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class ErrorIntercept implements HttpInterceptor {
  constructor(private logger: NGXLogger) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((res: HttpErrorResponse) => {
        let errorMessage = ''
        if (res.error instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Error: ${res.error.message}`
        } else {
          // server-side error
          errorMessage = `Error Code: ${res.status}\nMessage: ${res.message}`
        }
        this.logger.warn(errorMessage)
        return throwError(res)
      }),
    )
  }
}
