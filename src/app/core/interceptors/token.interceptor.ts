import { AuthService } from './../services/auth.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpParams, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParamsModel } from '@app/types/commonType';
@Injectable()
export class TokenIntercept implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.authService.getToken();
    console.log(req);
    req = req.clone({
      setHeaders: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
        charset: 'UTF-8',
      },
      params: req.params.append('token', token),
    });
    return next.handle(req);
  }
}
