import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenIntercept } from './token.interceptor';
import { ErrorIntercept } from './error.interceptor';
import { RxIntercept } from './rx.interceptor';
import { CustomJsonInterceptor, CustomJsonParser, JsonParser } from './json-interceptor';
import { NoopInterceptor } from './noop-interceptor';
import { TrimNameInterceptor } from './trim-name-interceptor';
import { EnsureHttpsInterceptor } from './ensure-https-interceptor';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: NoopInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: CustomJsonInterceptor, multi: true },
  { provide: JsonParser, useClass: CustomJsonParser },
  { provide: HTTP_INTERCEPTORS, useClass: EnsureHttpsInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: TrimNameInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: TokenIntercept, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: ErrorIntercept, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: RxIntercept, multi: true },
];
