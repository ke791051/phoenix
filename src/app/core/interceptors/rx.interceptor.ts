import { RxConstant } from './../constants/rx.constant'
import { AppService } from './../services/app.service'
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, throwError } from 'rxjs'
import { tap, catchError } from 'rxjs/operators'

@Injectable()
export class RxIntercept implements HttpInterceptor {
  constructor(private appService: AppService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap((res: any) => {
        if (res.status === 200) {
          this.appService.setCommandBus(RxConstant.handleRestAPISuccess.cmd, res)
        }
      }),
      catchError(resp => {
        // Log error via rxCommand because of circular dependency
        if (resp.error.status !== 401) {
          this.appService.setCommandBus(RxConstant.handleRestAPIError.cmd, resp)
        }
        return throwError(resp)
      }),
    )
  }
}
