import { SearchBarComponent } from 'src/app/components/search-bar/search-bar.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressBarComponent } from 'src/app/components/address-bar/address-bar.component';
import { PromoBannerComponent } from 'src/app/components/promo-banner/promo-banner.component';
import { BlockInitAnimateDirective } from './directives/block-init-animate.directive';
import { CarouselComponent } from 'src/app/components/carousel/carousel.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { CardWidgetComponent } from 'src/app/components/card-widget/card-widget.component';
import { CtaComponent } from 'src/app/components/cta/cta.component';
import { LazyImgDirective } from './directives/lazy-img.directive';
import { ItemWidgetComponent } from 'src/app/components/item-widget/item-widget.component';
import { SwiperModule } from 'swiper/angular';
import { AmountFilterPipe } from './pipes/amountFilter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderTextComponent } from 'src/app/components/header-text/header-text.component';
import { PhoneMaskDirective } from './directives/phone-mask.directive';
import { NgbdModalContentComponent } from 'src/app/components/ngbd-modal-content/ngbd-modal-content.component';
import { CheckScrollingDirective } from './directives/check-scrolling.directive';
import { NgbdModalOtpComponent } from 'src/app/components/ngbd-modal-otp/ngbd-modal-otp.component';
import { OtpDirective } from './directives/otp.directive';
import { TitleItemComponent } from 'src/app/components/title-item/title-item.component';
import { RouterModule } from '@angular/router';
import { SingleFieldModalComponent } from 'src/app/components/single-field-modal/single-field-modal.component';
import { AlertComponent } from 'src/app/components/alert/alert.component';
import { TwoFieldModalComponent } from 'src/app/components/two-field-modal/two-field-modal.component';
import { NgbdDropDownComponent } from 'src/app/components/ngbd-drop-down/ngbd-drop-down.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CardComponent } from 'src/app/components/_shared/card/card.component';
import { FormComponent } from 'src/app/components/_shared/form/form.component';
import { ItemComponent } from 'src/app/components/_shared/item/item.component';
import { SlideComponent } from 'src/app/components/_shared/slide/slide.component';
import { arrowIcons } from 'src/app/svg/arrow';
import { appBackIcon } from 'src/app/svg/back';
import { appBellPinIcon } from 'src/app/svg/bellPin';
import { brandIcons } from 'src/app/svg/brand';
import { cardIcons } from 'src/app/svg/card';
import { appCloseIcon } from 'src/app/svg/close';
import { appCreditCardIcon } from 'src/app/svg/creditCard';
import { appFbIcon } from 'src/app/svg/fb';
import { hotIcons } from 'src/app/svg/hot';
import { appLabelIcon } from 'src/app/svg/label';
import { appLocationIcon } from 'src/app/svg/location';
import { appLocationArrowIcon } from 'src/app/svg/locationArrow';
import { appLogoutIcon } from 'src/app/svg/logout';
import { mobileIcons } from 'src/app/svg/mobile';
import { appPaperIcon } from 'src/app/svg/paper';
import { appPlusIcon } from 'src/app/svg/plus';
import { appUserIcon } from 'src/app/svg/user';
import { SvgIconsModule } from '@ngneat/svg-icon';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { ChipsComponent } from '../components/chips/chips.component';
import { FooterComponent } from '../components/footer/footer.component';
import { ImageFallbackDirective } from './directives/image-fallback.directive';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { CardSkeletonDirective } from './directives/card-skeleton.directive';
import { ModalComponent } from '../components/modal/modal.component';
import { AddressComponent } from '../components/modal/address/address.component';
import { DeliveryTimePipe } from './pipes/delivery-time.pipe';
import { DeliveryLocatePipe } from './pipes/delivery-locate.pipe';
import { DeliveryOpeningTimePipe } from './pipes/delivery-opening-time.pipe';
import { MenuComponent } from '../components/menu/menu.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { ProductComponent } from '../components/product/product.component';
import { ProductOptionPricePipe } from './pipes/product-option-price.pipe';
import { PromotionComponent } from '../components/promotion/promotion.component';
import { appSearchIcon } from '@app/svg/search';
import { appCartIcon } from '@app/svg/cart';
import { appPhoneIcon } from '@app/svg/phone';
import { AccountAddressComponent } from '../components/modal/account-address/account-address.component';
import { OrderingComponent } from '../components/modal/ordering/ordering.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

@NgModule({
  declarations: [
    TitleItemComponent,
    LazyImgDirective,
    BlockInitAnimateDirective,
    PhoneMaskDirective,
    CheckScrollingDirective,
    OtpDirective,
    AmountFilterPipe,
    SearchBarComponent,
    AddressBarComponent,
    SearchBarComponent,
    PromoBannerComponent,
    CarouselComponent,
    CardWidgetComponent,
    CtaComponent,
    ItemWidgetComponent,
    HeaderTextComponent,
    NgbdModalContentComponent,
    NgbdModalOtpComponent,
    TitleItemComponent,
    SingleFieldModalComponent,
    TwoFieldModalComponent,
    AlertComponent,
    NgbdDropDownComponent,
    CardComponent,
    ItemComponent,
    SlideComponent,
    FormComponent,
    NavbarComponent,
    ChipsComponent,
    FooterComponent,
    ImageFallbackDirective,
    CardSkeletonDirective,
    ModalComponent,
    AddressComponent,
    DeliveryTimePipe,
    DeliveryLocatePipe,
    DeliveryOpeningTimePipe,
    MenuComponent,
    ProductComponent,
    ProductOptionPricePipe,
    PromotionComponent,
    AccountAddressComponent,
    OrderingComponent,
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbCarouselModule,
    SwiperModule,
    RouterModule,
    NgScrollbarModule,
    GooglePlaceModule,
    NgxSkeletonLoaderModule.forRoot({ animation: 'pulse', loadingText: 'This item is actually loading...' }),
    SvgIconsModule.forRoot({
      icons: [
        ...mobileIcons,
        ...arrowIcons,
        ...cardIcons,
        ...hotIcons,
        ...brandIcons,
        appCloseIcon,
        appBackIcon,
        appFbIcon,
        appLocationIcon,
        appBellPinIcon,
        appCreditCardIcon,
        appPlusIcon,
        appLogoutIcon,
        appLabelIcon,
        appUserIcon,
        appPaperIcon,
        appLocationArrowIcon,
        appSearchIcon,
        appCartIcon,
        appPhoneIcon,
      ],
    }),
  ],
  exports: [
    LazyImgDirective,
    BlockInitAnimateDirective,
    PhoneMaskDirective,
    CheckScrollingDirective,
    OtpDirective,
    AmountFilterPipe,
    SearchBarComponent,
    AddressBarComponent,
    SearchBarComponent,
    PromoBannerComponent,
    CarouselComponent,
    CardWidgetComponent,
    CtaComponent,
    ItemWidgetComponent,
    HeaderTextComponent,
    TitleItemComponent,
    SingleFieldModalComponent,
    CardComponent,
    ItemComponent,
    SlideComponent,
    FormComponent,
    NavbarComponent,
    FooterComponent,
    ChipsComponent,
    NgxSkeletonLoaderModule,
    SvgIconsModule,
    DeliveryTimePipe,
    DeliveryLocatePipe,
    DeliveryOpeningTimePipe,
    MenuComponent,
    NgScrollbarModule,
    ModalComponent,
    ImageFallbackDirective,
    PromotionComponent,
    AccountAddressComponent,

    GooglePlaceModule,
  ],
})
export class SharedModule {}
