import { Observable } from 'rxjs';
import { dataObject, dataType } from 'src/app/types/data.type';
import { HttpOption } from 'src/app/core/models/options';
import { ApiResponse } from '../models/api';

export interface IHttpService {
  get<T>(url: string, param?: dataObject): Observable<ApiResponse<T>>;
  post<T>(url: string, body: dataType, param?: dataObject): Observable<ApiResponse<T>>;
  put<T>(url: string, req: HttpReq, customOption?: HttpOption): Observable<ApiResponse<T>>;
  delete(url: string, key: string): Observable<ApiResponse<unknown>>;
}

export type HttpReq = {
  headers?: dataType;
  queryObject?: dataType;
  body?: dataType;
  baseURL?: string;
};

export type MockDefaultData<T> = {
  url: string;
  data: T[];
};
