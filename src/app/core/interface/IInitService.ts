import { Observable } from 'rxjs';

export interface IInitService {
  hasLoginSuccess: boolean;
  hasOtpValidated: boolean;
  hasPrivacyAccepted: boolean;

  handleLoginSuccess(status: boolean): Observable<boolean>;
  handleOtpValidated(status: boolean): Observable<boolean>;
  handlePrivacyAccepted(status: boolean): Observable<boolean>;
}

export type Init = {
  hasLoginSuccess: boolean;
  hasOtpValidated: boolean;
  hasPrivacyAccepted: boolean;
};
