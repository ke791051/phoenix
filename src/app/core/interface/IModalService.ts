import { BehaviorSubject } from 'rxjs';
import { ModalOptionModel, ModalVisibleModel } from '../models/modal.model';

export interface IModalService {
  optionSub$: BehaviorSubject<ModalOptionModel>;
  visibleSub$: BehaviorSubject<ModalVisibleModel>;
}
