import { BehaviorSubject, Observable } from 'rxjs';
import { StorageEventModel, StorageType } from '../models/storage.model';

export interface IStorageService {
  changeEvent: BehaviorSubject<StorageEventModel>;
  changeEvent$: Observable<StorageEventModel>;
  flushStorage(type: StorageType): void;
  get(type: StorageType, key: string): string;
  set(type: StorageType, key: string, data: any): boolean;
}
