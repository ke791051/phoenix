import { Observable } from 'rxjs';
import { CartProductModel } from 'src/app/core/models/product.model';
import { EventMessage } from '../classes/EventMessage';

export interface ICartService {
  itemList: CartProductModel[];
  create(item: CartProductModel): Observable<EventMessage<CartProductModel[]>>;
  modify(item: CartProductModel, count: number, mark: string): Observable<EventMessage<CartProductModel[]>>;
  clean(): void;
  // handleOrder(): Observable<EventMessage<PreOrderItemModel[]>>;
}
