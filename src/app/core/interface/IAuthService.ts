import { Observable } from 'rxjs';
import { ApiResponse } from '../models/api';
import { AuthModel } from '../models/auth.model';
import { IUser } from '../models/user.modal';

export interface IAuthService {
  account$: Observable<AuthModel>;
  getToken(): string;
  logIn(payload: SignInPayLoad): void;
  fbLogin(): void;
  fbLogout(): void;
  apiAuthenticate(accessToken: string): Observable<ApiResponse<AuthModel>>;
  signUp(payload: SignUpPayLoad): Observable<ApiResponse<IUser>>;
  logout(): void;
}

export type SignUpPayLoad = {
  email: string;
  password: string;
};

export type SignInPayLoad = SignUpPayLoad;
