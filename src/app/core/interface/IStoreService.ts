import { Observable, Subject } from 'rxjs';
import { StoreModel } from 'src/app/core/models/store.model';

export interface IStoreService {
  itemList: StoreModel[];
  filterList: StoreModel[];
  currentItem: StoreModel;
  currentItemSub$: Observable<StoreModel>;
  changeEvent$: Subject<StoreModel[]>;
  fetchData(address: string): Observable<StoreModel[] | string>;
  getStore(id: string): void;
  getStoreById(id: string): StoreModel | undefined;
  getFilterList(payload: string): void;
}

export type StoreReq = {
  address?: string;
  store_id?: string;
};
