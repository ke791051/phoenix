import { dataType } from 'src/app/types/data.type';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
export interface IAppService {
  // promoText: BehaviorSubject<PromoModel>;
  promoText$: Observable<PromoModel>;

  // currentPage: BehaviorSubject<string>;
  currentPage$: Observable<string>;

  // globalMask: BehaviorSubject<boolean>;
  globalMask$: Observable<boolean>;

  // navbarActive: BehaviorSubject<boolean>;
  navbarActive$: Observable<boolean>;

  // getCommandBus$(): Observable<PromoModel>;
  // setCommandBus(cmd: string, data: dataType): void;

  toggleNavbarEvent(newStatus: boolean): void;
  handlePromoTextChange(newStatus: PromoModel): void;
  handleGlobalMaskChange(newStatus: boolean): void;
  handleCurrentPageChange(newStatus: string): void;
}

export type PromoModel = {
  show: boolean;
  txt?: string;
  redirectTo?: string;
};

export type CmdModel = { cmd: string; data: dataType };
