import { Observable } from 'rxjs';
import { CardModel } from '../models/cards.model';
import { EventMessageModel } from '../models/event-message.model';

export interface ICardService {
  getCard<T>(itemList: T[], formatFunc: (data: T) => CardModel): Observable<EventMessageModel<CardModel[]>>;
}
