import { Observable } from 'rxjs';
import { PopularModel } from '../models/popular.model';

export interface IPopularService {
  itemList: PopularModel[];
  fetchData(storeId: string): Observable<PopularModel[]>;
}

export type PopularReq = {
  token: string;
};
