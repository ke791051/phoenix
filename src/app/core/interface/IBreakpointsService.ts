import { Observable } from 'rxjs';

export interface IBreakpointsService {
  activeBreakpoints: string[];
  subscribeToLayoutChanges(): Observable<string>;
  setPlatform(res: string): number;
  isBreakpointActive(breakpointName: BreakpointName): boolean;
}

export type BreakpointName = 'xs' | 'sm' | 'md' | 'lg' | 'xl';
