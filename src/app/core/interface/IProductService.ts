import { Observable, Subject } from 'rxjs';
import { BrandProductModel, ProductModel } from 'src/app/core/models/product.model';
export interface IProductService {
  dataReadyEvent: Subject<boolean>;
  brandList: BrandProductModel[];
  brandListSub$: Observable<BrandProductModel[]>;
  fetchData(storeId: string, brandFunc?: (data: ProductModel[]) => BrandProductModel[]): void;
  handleDataClear(): void;
  get(pid: string): ProductModel | undefined;
}

export type ProductReq = {
  token: string;
  store_id: string;
};
