import { Injectable } from '@angular/core';
import { CartService } from './cart.service';
import { BehaviorSubject, Observable, Subject, Subscriber } from 'rxjs';
import {
  OrderResponseModel,
  OrderSearchPayload,
  OrderDirtyItemList,
  OrderResponseDirtyModel,
} from '../models/order.model';
import { OrderResponse } from '../classes/Order';
import { ApiResponse } from '../models/api';
import { HttpService } from './http/http.service';
import { dataObject } from '../../types/data.type';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private ReqEvent: BehaviorSubject<string>;
  public ReqEvent$: Observable<string>;
  public searchResult: OrderResponseModel[];
  public changeEvent$: Subject<OrderResponseModel[]>;
  constructor(private http: HttpService, private app: AppService) {
    this.ReqEvent = new BehaviorSubject('');
    this.ReqEvent$ = this.ReqEvent.asObservable();
    this.searchResult = [];
    this.changeEvent$ = new Subject();
  }

  public post(): void {}
  public fetchData(payload: OrderSearchPayload[]): Observable<OrderResponseModel[]> {
    return new Observable<OrderResponseModel[]>((sub: Subscriber<OrderResponseModel[] | string>) => {
      let param: dataObject = {};
      payload.forEach((item: OrderSearchPayload) => {
        let tmp: dataObject = {};
        tmp[item.key] = item.value;
        param = Object.assign({}, param, tmp);
      });
      this.http
        .get<OrderResponseDirtyModel[]>('https://dev.justkitchen.com/api/public/stores/getstores', param)
        .subscribe((res: ApiResponse<OrderResponseDirtyModel[]>) => {
          if (res.status) {
            const _order: OrderResponseModel[] = res.result.map(
              (item: OrderResponseDirtyModel) => new OrderResponse(item),
            );
            if (_order.length > 0) {
              this.searchResult = _order;
            }
            this.changeEvent$.next(this.searchResult);
            sub.next(this.searchResult);
          } else {
            this.app.errorEvent$.next(res.message);
            sub.error([]);
          }
          sub.complete();
        });
    });
  }

  private getOrderItem(): void {
    // const tmp:
  }
}
