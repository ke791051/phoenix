import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ModalOptionModel, ModalVisibleModel } from '../models/modal.model';
import { IModalService } from '../interface/IModalService';

@Injectable({
  providedIn: 'root',
})
export class ModalService implements IModalService {
  public optionSub$: BehaviorSubject<ModalOptionModel>;
  public visibleSub$: BehaviorSubject<ModalVisibleModel>;
  constructor() {
    this.optionSub$ = new BehaviorSubject<ModalOptionModel>({
      modalClass: 'modal-customer',
      backdropClass: 'modal-backdrop-customer',
    });
    this.visibleSub$ = new BehaviorSubject<ModalVisibleModel>({ visible: false });
  }
}
