import { Product } from 'src/app/core/classes/Product';
import { DirtyProduct, BrandProductModel, ProductModel } from 'src/app/core/models/product.model';
import { BehaviorSubject, Observable, Subscriber, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { IProductService, ProductReq } from 'src/app/core/interface/IProductService';
import { AuthService } from './auth.service';
import { HttpService } from './http/http.service';
import { ApiResponse } from '../models/api';
import { dataObject } from '@app/types/data.type';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class ProductsService implements IProductService {
  public brandList: BrandProductModel[];
  private productList: ProductModel[];
  public dataReadyEvent: Subject<boolean>;
  private brandListSub: BehaviorSubject<BrandProductModel[]>;
  public brandListSub$: Observable<BrandProductModel[]>;
  constructor(private http: HttpService, private auth: AuthService) {
    this.brandList = [];
    this.productList = [];
    this.dataReadyEvent = new Subject();
    this.brandListSub = new BehaviorSubject<BrandProductModel[]>([]);
    this.brandListSub$ = this.brandListSub.asObservable();
  }

  public fetchData(storeId: string, brandFunc?: (data: ProductModel[]) => BrandProductModel[]): void {
    const param: dataObject = {
      store_id: storeId,
    };
    this.http
      .post<DirtyProduct[]>('https://dev.justkitchen.com/api/public/stores/getproducts', {}, param)
      .pipe(untilDestroyed(this))
      .subscribe((res: ApiResponse<DirtyProduct[]>) => {
        if (res.status) {
          this.productList = res.result.map((item: DirtyProduct) => new Product(item));
          this.dataReadyEvent.next(true);
          this.brandList = brandFunc ? brandFunc(this.productList) : [{ brandName: 'All', list: this.productList }];
          this.brandListSub.next(this.brandList);
        }
      });
  }
  public handleDataClear(): void {
    this.brandList = [];
    this.productList = [];
    this.brandListSub.next([]);
  }
  public get(pid: string): ProductModel | undefined {
    return this.productList.find((item: ProductModel) => item.product_id === pid);
  }
}
