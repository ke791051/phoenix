import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IInitService, Init } from '../interface/IInitService';
import { HttpService } from './http/http.service';

@Injectable({
  providedIn: 'root',
})
export class InitService implements IInitService {
  public hasLoginSuccess: boolean;
  public hasOtpValidated: boolean;
  public hasPrivacyAccepted: boolean;

  get isInit(): boolean {
    return this.hasLoginSuccess && this.hasLoginSuccess && this.hasPrivacyAccepted;
  }
  constructor(private http: HttpService) {
    this.hasLoginSuccess = false;
    this.hasOtpValidated = false;
    this.hasPrivacyAccepted = false;
    this.getInit();
  }

  private getInit(): void {
    this.http.get<Init>('', {});
  }

  public handleLoginSuccess(status: boolean): Observable<boolean> {
    this.hasLoginSuccess = status;
    return of(this.hasLoginSuccess);
  }
  public handleOtpValidated(status: boolean): Observable<boolean> {
    this.hasOtpValidated = status;
    return of(this.hasOtpValidated);
  }
  public handlePrivacyAccepted(status: boolean): Observable<boolean> {
    this.hasPrivacyAccepted = status;
    return of(this.hasPrivacyAccepted);
  }
}
