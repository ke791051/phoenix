import { dataType } from '../../types/data.type';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { RxBusModel } from '../models/rx';
import { tap } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { CmdModel, PromoModel, IAppService } from '../interface/IAppService';
import { ErrorMsgEvent } from '../models/event-message.model';

@Injectable({
  providedIn: 'root',
})
export class AppService implements IAppService {
  private rxCommandBus: Subject<RxBusModel> = new Subject<RxBusModel>();
  private rxCommandBus$: Observable<RxBusModel> = this.rxCommandBus.asObservable();

  // PromoBannerComponent => PromoBannerComponent
  private promoText: BehaviorSubject<PromoModel>;
  public promoText$: Observable<PromoModel>;
  // PagesComponent => MobileBottomNavComponent
  private currentPage: BehaviorSubject<string>;
  public currentPage$: Observable<string>;

  private globalMask: BehaviorSubject<boolean>;
  public globalMask$: Observable<boolean>;

  private navbarActive: BehaviorSubject<boolean>;
  public navbarActive$: Observable<boolean>;

  public errorEvent$: Subject<string | ErrorMsgEvent>;

  constructor(private logger: NGXLogger) {
    this.promoText = new BehaviorSubject<PromoModel>({ show: false, txt: '', redirectTo: '' });
    this.promoText$ = this.promoText.asObservable();
    this.currentPage = new BehaviorSubject<string>('');
    this.currentPage$ = this.currentPage.asObservable();
    this.globalMask = new BehaviorSubject<boolean>(false);
    this.globalMask$ = this.globalMask.asObservable();
    this.navbarActive = new BehaviorSubject<boolean>(false);
    this.navbarActive$ = this.navbarActive.asObservable();

    this.navbarActive$.subscribe(res => console.log(res));
    this.errorEvent$ = new Subject();
  }

  public getCommandBus$(): Observable<CmdModel> {
    return this.rxCommandBus$.pipe(tap((res: RxBusModel) => this.logger.debug('[CommandBus]', res)));
  }

  public setCommandBus(cmd: string, data: dataType): void {
    this.rxCommandBus.next({ cmd, data });
  }

  public toggleNavbarEvent(newStatus: boolean): void {
    this.globalMask.next(newStatus);
    this.navbarActive.next(newStatus);
  }

  public handlePromoTextChange(newStatus: PromoModel): void {
    this.promoText.next(newStatus);
  }

  public handleGlobalMaskChange(newStatus: boolean): void {
    this.globalMask.next(newStatus);
  }

  public handleCurrentPageChange(newStatus: string): void {
    this.currentPage.next(newStatus);
  }
}
