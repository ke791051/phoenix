import { DOCUMENT } from '@angular/common'
import { Inject, Injectable, Renderer2, RendererFactory2 } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class LoadscriptService {
  public hasLoadScript = false
  private renderer: Renderer2
  constructor(@Inject(DOCUMENT) private document: Document, public rendererFactory: RendererFactory2) {
    // get an instance of Renderer2
    this.renderer = rendererFactory.createRenderer(null, null)
  }

  public loadScript(url: string, placed?: 'body' | 'head'): Promise<void> {
    return new Promise((resolve, reject) => {
      const script = this.renderer.createElement('script')
      script.src = url
      script.onload = resolve
      script.onerror = reject
      if (placed === 'head') {
        this.renderer.appendChild(this.document.head, script)
      } else {
        this.renderer.appendChild(this.document.body, script)
      }
      this.hasLoadScript = true
    })
  }
}
