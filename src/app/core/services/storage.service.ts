import { Injectable } from '@angular/core';
import { has, get } from 'lodash';
import { NGXLogger } from 'ngx-logger';
import { Observable, BehaviorSubject } from 'rxjs';
import { WebStorage, WebStorageModel } from '../classes/Storage';
import { IStorageService } from '../interface/IStorageService';
import { StorageEventModel, StorageType } from '../models/storage.model';

@Injectable({
  providedIn: 'root',
})
export class StorageService implements IStorageService {
  public changeEvent: BehaviorSubject<StorageEventModel>;
  public changeEvent$: Observable<StorageEventModel>;
  public window: any;
  constructor(private logger: NGXLogger) {
    if (!!window) {
      this.window = window;
    } else {
      this.window = {
        localStorage: new WebStorage(),
        sessionStorage: new WebStorage(),
      };
    }
    this.changeEvent = new BehaviorSubject(new WebStorageModel());
    this.changeEvent$ = this.changeEvent.asObservable();
  }

  public flushStorage(type: StorageType): void {
    if (has(this.window, type)) {
      try {
        this.window[type].clear();
      } catch (error) {
        this.logger.error('Failed to flush local storage item', error);
      }
    }
  }

  public get(type: StorageType, key: string): string {
    if (key && key !== '') {
      const str: string | null = this.window[type].getItem(key);
      if (str === null) return '';
      return str;
    }
    return '';
  }

  public set(type: StorageType, key: string, data: any): boolean {
    if (key && key !== '') {
      let str: string;
      if (typeof data === 'object' && data !== null) {
        str = JSON.stringify(data);
      } else {
        str = data;
      }
      window[type].setItem(key, str);
      const tmp: StorageEventModel = new WebStorageModel({ type, key, str });
      this.changeEvent.next(tmp);
      return true;
    }
    return false;
  }
}
