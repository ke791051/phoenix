import { HttpClient, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';
import { catchError, throwError, Observable, Subscriber } from 'rxjs';
import { IHttpService, MockDefaultData } from 'src/app/core/interface/IHttpService';
import { environment } from '@env/environment';
import { HttpService } from './http.service';
import { ApiResponse } from '@app/core/models/api';
import { dataObject, dataType } from '@app/types/data.type';

export class HttpClientService extends HttpService implements IHttpService {
  private baseUrl: string;
  public setDefaultResData<T>(payload: MockDefaultData<T>): void {
    return;
  }

  constructor(private http: HttpClient) {
    super();
    this.baseUrl = environment.apiUrl;
  }

  public get<T>(url: string, param?: dataObject): Observable<ApiResponse<T>> {
    const urlParam: HttpParams = this.setURLParams(param);
    const options = {
      observe: 'response' as const,
      params: urlParam,
    };
    return this.getHttpBody<T>(this.http.get<ApiResponse<T>>(`${url}`, options));
  }

  public post<T>(url: string, body: dataType, param?: dataObject): Observable<ApiResponse<T>> {
    const urlParam: HttpParams = this.setURLParams(param);
    const options = {
      observe: 'response' as const,
      params: urlParam,
    };
    return this.getHttpBody<T>(this.http.post<ApiResponse<T>>(`${url}`, body, options));
  }

  public put<T>(url: string, body: dataType): Observable<ApiResponse<T>> {
    const options = {
      observe: 'response' as const,
    };
    return this.getHttpBody<T>(this.http.put<ApiResponse<T>>(`${url}`, body, options));
  }

  public delete<T>(url: string, key: string): Observable<ApiResponse<T>> {
    const options = {
      observe: 'response' as const,
    };
    return this.getHttpBody<T>(this.http.delete<ApiResponse<T>>(`${url}/${key}`, options));
  }

  private getHttpBody<T>(payload: Observable<HttpResponse<ApiResponse<T>>>): Observable<ApiResponse<T>> {
    return new Observable<ApiResponse<T>>((sub: Subscriber<ApiResponse<T>>) => {
      payload.pipe(catchError(this.handleErrors)).subscribe((res: HttpResponse<ApiResponse<T>>) => {
        if (res.ok && res.body) {
          sub.next(res.body);
          sub.complete();
        }
        sub.error();
        sub.complete();
      });
    });
  }

  private setURLParams(obj?: dataObject): HttpParams {
    let params = new HttpParams();
    if (!obj) {
      return params;
    }

    for (const key in obj) {
      params = params.set(key, obj[key].toString());
    }
    return params;
  }

  private handleErrors(err: HttpErrorResponse): Observable<never> {
    if (err.status === 400) {
      console.error('Bad Request', err);
    }
    if (err.status === 401) {
      console.error('Unauthorized', err);
    }
    if (err.status === 403) {
      console.error('Forbidden', err);
    }
    if (err.status === 404) {
      console.error('Not Found', err);
    }
    if (err.status === 405) {
      console.error('Method Not Allowed', err);
    }
    if (err.status === 500) {
      console.error('Internal Server Error', err);
    }
    if (err.status === 503) {
      console.error('Service Unavailable', err);
    }
    if (err.status === 504) {
      console.error('Gateway Timeout', err);
    }
    return throwError(() => err);
  }
}
