import { HttpReq, MockDefaultData } from '@app/core/interface/IHttpService';
import { ApiResponse } from '@app/core/models/api';
import { HttpOption } from '@app/core/models/options';
import { dataObject, dataType } from '@app/types/data.type';
import { Observable, of } from 'rxjs';
import { IHttpService } from '../../interface/IHttpService';
import { HttpService } from './http.service';

export class HttpMockService extends HttpService implements IHttpService {
  public setDefaultResData<T>(payload: MockDefaultData<T>) {
    this._defaultResData.push(payload);
  }
  constructor() {
    super();
  }

  public get<T>(url: string, param?: dataObject): Observable<ApiResponse<T>> {
    return of(this.getData(url));
  }
  public post<T>(url: string, body: dataType, param?: dataObject): Observable<ApiResponse<T>> {
    return of(this.getData(url));
  }
  public put<T>(url: string, req: HttpReq, customOption?: HttpOption): Observable<ApiResponse<T>> {
    return of(this.getData(url));
  }
  public delete(url: string, key: string): Observable<ApiResponse<unknown>> {
    return of(this.getData(url));
  }

  private getData(url: string): ApiResponse<any> {
    const check: MockDefaultData<unknown> | undefined = this._defaultResData.find(
      (item: MockDefaultData<unknown>) => item.url === url,
    );
    const res: ApiResponse<any> = {
      status: !!check,
      result: check ? check.data : null,
      message: '',
    };
    return res;
  }
}
