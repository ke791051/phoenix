import { Injectable } from '@angular/core';
import { HttpReq, IHttpService, MockDefaultData } from '@app/core/interface/IHttpService';
import { ApiResponse } from '@app/core/models/api';
import { AuthModel } from '@app/core/models/auth.model';
import { HttpOption } from '@app/core/models/options';
import { CartProductModel, ProductModel } from '@app/core/models/product.model';
import { StoreModel } from '@app/core/models/store.model';
import { IUser } from '@app/core/models/user.modal';
import { dataObject, dataType } from '@app/types/data.type';
import { Observable } from 'rxjs';
@Injectable()
export abstract class HttpService implements IHttpService {
  protected _defaultResData: MockDefaultData<IUser | StoreModel | ProductModel | CartProductModel | AuthModel>[];
  abstract setDefaultResData<T>(payload: MockDefaultData<T>): void;

  constructor() {
    this._defaultResData = [];
  }

  abstract get<T>(url: string, param?: dataObject): Observable<ApiResponse<T>>;
  abstract post<T>(url: string, body: dataType, param?: dataObject): Observable<ApiResponse<T>>;
  abstract put<T>(url: string, req: HttpReq, customOption?: HttpOption): Observable<ApiResponse<T>>;
  abstract delete(url: string, key: string): Observable<ApiResponse<unknown>>;
}
