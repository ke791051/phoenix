import { Injectable } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JsonObject } from 'src/app/types/commonType';
import { getString } from '../utils/lodash.function';
import { get } from 'lodash';
import { BreakpointName, IBreakpointsService } from '../interface/IBreakpointsService';

export const BreakpointConstant: JsonObject<string> = {
  ExtraSmall: 'xs',
  Small: 'sm',
  Medium: 'md',
  Large: 'lg',
  ExtraLarge: 'xl',
  ExtraExtraLarge: 'xxl',
};

@Injectable({
  providedIn: 'root',
})
export class BreakpointsService implements IBreakpointsService {
  public activeBreakpoints: string[];
  private breakpoints: Record<string, string>;

  constructor(private breakpointObserver: BreakpointObserver) {
    this.activeBreakpoints = [];
    this.breakpoints = {
      '(max-width: 575.99px)': getString(BreakpointConstant, 'ExtraSmall', ''),
      '(min-width: 576px) and (max-width:767.99px)': getString(BreakpointConstant, 'Small', ''),
      '(min-width: 768px) and (max-width:991.99px)': getString(BreakpointConstant, 'Medium', ''),
      '(min-width: 992px) and (max-width:1199.99px)': getString(BreakpointConstant, 'Large', ''),
      '(min-width: 1200px) and (max-width:1399.99px)': getString(BreakpointConstant, 'ExtraLarge', ''),
      '(min-width: 1400px))': getString(BreakpointConstant, 'ExtraExtraLarge', ''),
    };
  }

  public subscribeToLayoutChanges<T>(): Observable<T> {
    return this.breakpointObserver.observe(this.getBreakpoints()).pipe(
      map((observeResponse: BreakpointState) => {
        const parseBreakpointsRes: string[] = this.parseBreakpointsResponse(observeResponse.breakpoints);
        return get(parseBreakpointsRes, '0', '');
      }),
    );
  }

  public setPlatform(res: string): number {
    console.log(res);
    if (res === 'sm' || res === 'xs') {
      return 2; // mobile
    } else if (res === 'md') {
      return 1; // tablet
    } else {
      return 0; // desktop
    }
  }

  public isBreakpointActive(breakpointName: BreakpointName): boolean {
    const isActive: boolean = this.activeBreakpoints.indexOf(breakpointName) > -1;
    return isActive;
  }

  private getBreakpoints(): string[] {
    return Object.keys(this.breakpoints);
  }

  private getBreakpointName(breakpointValue: string): string {
    return getString(this.breakpoints, breakpointValue, '');
  }

  private parseBreakpointsResponse<T>(breakpoints: Record<string, boolean>): string[] {
    this.activeBreakpoints = [];

    Object.keys(breakpoints).map(key => {
      if (breakpoints[`${key}`]) {
        this.activeBreakpoints.push(this.getBreakpointName(`${key}`));
      }
    });

    return this.activeBreakpoints;
  }
}
