import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import { EventMessage } from '../classes/EventMessage';
import { get, has } from 'lodash';
import { google } from 'google-maps';
import { GoogleLatLngBounds } from '../models/google.model';
import { StorageService } from './storage.service';
import parseJson from 'parse-json';
import { StorageType } from '../models/storage.model';

@Injectable({
  providedIn: 'root',
})
export class GoogleMapService {
  public google?: google;
  public currentAddress: string;
  public currentWholeAddress: string;
  public currentLng: number;
  public currentLat: number;
  private defaultBounds: GoogleLatLngBounds;
  private geoCoder?: google.maps.Geocoder;
  public zoom: number;
  private apiSub: BehaviorSubject<string>;
  public apiSub$: Observable<string>;
  public map?: google.maps.Map;
  private addressSub: BehaviorSubject<string>;
  public addressSub$: Observable<string>;

  constructor(private storage: StorageService) {
    this.currentAddress = '';
    this.currentWholeAddress = '';
    this.currentLng = 0;
    this.currentLat = 0;
    this.zoom = 0;
    this.apiSub = new BehaviorSubject<string>('');
    this.apiSub$ = this.apiSub.asObservable();
    this.defaultBounds = {
      north: 0,
      south: 0,
      east: 0,
      west: 0,
    };
    this.addressSub = new BehaviorSubject<string>('');
    this.addressSub$ = this.addressSub.asObservable();
  }

  public handleGoogleUpdate(newItem: google): void {
    this.google = newItem;
  }

  public fetchGeocodeLatLng(lat: number = this.currentLat, lng: number = this.currentLng): void {
    if (this.storage.get(StorageType.local, 'bounds')) {
      const storeBoundsStr: string | null = this.storage.get(StorageType.local, 'bounds');
      let storeBounds: GoogleLatLngBounds = { north: 0, south: 0, east: 0, west: 0 };
      if (storeBoundsStr !== '') {
        storeBounds = parseJson(storeBoundsStr);
      }

      if (this.boundsCheck(lat, lng, storeBounds) && this.storage.get(StorageType.local, 'geoCode')) {
        const geoCodeStr: string | null = this.storage.get(StorageType.local, 'geoCode');
        let geoCode: google.maps.GeocoderResult =
          geoCodeStr !== '' ? parseJson(geoCodeStr) : new google.maps.Geocoder();
        this.currentAddress = this.rebuildAddress(geoCode.address_components);
      }
      return;
    }
    this.geoCoder = new google.maps.Geocoder();
    this.geoCoder.geocode(
      { location: { lat, lng } },
      (res: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus) => {
        if (status === 'OK') {
          if (has(res, '0')) {
            this.zoom = 12;
            const geoCode: google.maps.GeocoderResult = get(res, '0');
            this.currentAddress = this.rebuildAddress(geoCode.address_components);
            this.currentWholeAddress = this.rebuildWholeAddress(geoCode.address_components);
            this.storage.set(StorageType.local, 'bounds', `${JSON.stringify(this.defaultBounds)}`);
            this.storage.set(StorageType.local, 'geoCode', `${JSON.stringify(geoCode)}`);
          } else {
            this.apiSub.error('There is no available address.');
          }
          {
          }
        } else {
          this.apiSub.error('Geocoder failed due to: ' + status);
        }
      },
    );
  }

  public getLocation(): Observable<EventMessage<string>> {
    return new Observable<EventMessage<string>>((sub: Subscriber<EventMessage<string>>) => {
      if (!('geolocation' in navigator)) {
        sub.error(new EventMessage(false, 'Geolocation is not supported in this browser.'));
        sub.complete();
      }

      navigator.geolocation.getCurrentPosition(
        (position: GeolocationPosition) => {
          this.currentLng = position.coords.longitude;
          this.currentLat = position.coords.latitude;
          const defaultBounds: GoogleLatLngBounds = {
            north: this.currentLat + 0.01,
            south: this.currentLat - 0.01,
            east: this.currentLng + 0.01,
            west: this.currentLng - 0.01,
          };
          this.defaultBounds = defaultBounds;
          sub.next(new EventMessage(true));
          sub.complete();
        },
        (error: GeolocationPositionError) => {
          sub.error(new EventMessage(false, error.message));
          sub.complete();
        },
      );
    });
  }

  public getAutocompleteOption(): google.maps.places.AutocompleteOptions {
    const otps: google.maps.places.AutocompleteOptions = {
      componentRestrictions: { country: ['zh-TW'] },
      fields: ['address_components', 'geometry'],
      types: ['address'],
      bounds: this.defaultBounds,
    };
    return otps;
  }

  public getGeoCodeStorage(): void {
    if (this.storage.get(StorageType.local, 'geoCode')) {
      const geoCodeStr: string | null = this.storage.get(StorageType.local, 'geoCode');
      const storeGeoCode: google.maps.GeocoderResult =
        geoCodeStr !== '' ? parseJson(geoCodeStr) : new google.maps.Geocoder();
      this.currentAddress = this.rebuildAddress(storeGeoCode.address_components);
      this.currentWholeAddress = this.rebuildWholeAddress(storeGeoCode.address_components);
    }
  }

  private boundsCheck(lat: number, lng: number, storeBounds: GoogleLatLngBounds): boolean {
    const { north, south, east, west } = storeBounds;
    return north >= lat && lat >= south && east >= lng && lng >= west;
  }

  private rebuildAddress(payload: google.maps.GeocoderAddressComponent[]): string {
    let address = '';
    for (const component of payload) {
      // @ts-ignore remove once typings fixed
      const componentType = component.types[0];

      switch (componentType) {
        case 'street_number': {
          address = `${component.long_name} ${address}`;
          break;
        }

        case 'route': {
          address += component.short_name;
          break;
        }

        default: {
          break;
        }
      }
    }
    this.addressSub.next(address);
    return address;
  }
  private rebuildWholeAddress(payload: google.maps.GeocoderAddressComponent[]): string {
    let address: string[] = [];
    for (const component of payload) {
      // @ts-ignore remove once typings fixed
      const componentType = component.types[0];

      switch (componentType) {
        case 'street_number': {
          address.push(`${component.long_name} ${address}`);
          break;
        }

        case 'route': {
          address.push(component.short_name);
          break;
        }
        case 'administrative_area_level_4':
        case 'administrative_area_level_3':
        case 'administrative_area_level_1': {
          address.push(component.short_name);
          break;
        }

        default: {
          break;
        }
      }
    }

    // this.addressSub.next(address);
    return address.reverse().reduce((prev: string, curr: string) => prev + curr);
  }
}
