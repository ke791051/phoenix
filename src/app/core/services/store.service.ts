import { Injectable } from '@angular/core';
import { Observable, Subscriber, take, BehaviorSubject, Subject } from 'rxjs';
import { IStoreService, StoreReq } from 'src/app/core/interface/IStoreService';
import { DirtyProduct } from 'src/app/core/models/product.model';
import { StoreModel, DirtyStore, Week } from 'src/app/core/models/store.model';
import { HttpService } from './http/http.service';
import { Store } from 'src/app/core/classes/Store';
import { HttpReq } from 'src/app/core/classes/HttpReq';
import { get, isString } from 'lodash';
import { ApiResponse } from '../models/api';
import { dataObject } from '@app/types/data.type';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class StoreService implements IStoreService {
  public itemList: StoreModel[];
  public filterList: StoreModel[];
  public currentItem: StoreModel;
  private currentItemSub: BehaviorSubject<StoreModel>;
  public currentItemSub$: Observable<StoreModel>;
  private currentAddress: string;
  public changeEvent$: Subject<StoreModel[]>;
  public isOpen: boolean;

  constructor(private http: HttpService, private app: AppService) {
    this.itemList = [];
    this.filterList = [];
    this.currentAddress = '';
    this.currentItem = new Store();
    this.currentItemSub = new BehaviorSubject<StoreModel>(this.currentItem);
    this.currentItemSub$ = this.currentItemSub.asObservable();
    this.changeEvent$ = new Subject();
    this.isOpen = false;
  }

  public fetchData(address: string): Observable<StoreModel[]> {
    return new Observable<StoreModel[]>((sub: Subscriber<StoreModel[] | string>) => {
      if (this.currentAddress === address && this.itemList.length > 0) {
        sub.next(this.itemList);
      }
      const body: StoreReq = {
        address: address,
      };
      const req: HttpReq = new HttpReq({ body });
      this.http
        .post<DirtyProduct[]>('https://dev.justkitchen.com/api/public/stores/getstores', req, {
          address: address,
        })
        .subscribe((res: ApiResponse<DirtyStore[]>) => {
          if (res.status) {
            this.itemList = res.result.map((item: DirtyStore) => new Store(item));
            this.changeEvent$.next(this.itemList);
            sub.next(this.itemList);
          } else {
            this.app.errorEvent$.next(res.message);
            sub.error([]);
          }
          sub.complete();
        });
    });
  }

  private fetchDataById(id: string): Observable<StoreModel | string> {
    return new Observable<StoreModel>((sub: Subscriber<StoreModel | string>) => {
      const param: dataObject = {
        store_id: id,
      };
      this.http
        .post<DirtyProduct[]>('https://dev.justkitchen.com/api/public/stores/getbyid', {}, param)
        .pipe(take(1))
        .subscribe((res: ApiResponse<DirtyStore[]>) => {
          if (res.status) {
            const data: DirtyStore = get(res, 'result.0');
            this.currentItem = new Store(data);
            sub.next(this.currentItem);
          } else {
            sub.error(res.message);
          }
          sub.complete();
        });
    });
  }

  public getStore(id: string): void {
    if (this.itemList.length > 0) {
      const target: StoreModel | undefined = this.itemList.find((item: StoreModel) => item.store_id === id);
      if (target) {
        this.currentItem = target;
        this.currentItemSub.next(this.currentItem);
        const today = new Date().getDay() - 1;
        const weekMapList: string[] = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        const weekMap: string = weekMapList[today];
        const weekData: Week | undefined = target.week.find((item: Week) => item.day === weekMap);
        if (!!weekData) {
          this.isOpen = weekData.isOpen;
        }
        return;
      }
    }
    this.fetchDataById(id)
      .pipe(take(1))
      .subscribe((item: StoreModel | string) => {
        if (!isString(item)) {
          this.currentItem = item;
          this.currentItemSub.next(this.currentItem);
        }
      });
  }

  public getStoreById(id: string): StoreModel | undefined {
    return this.itemList.find((item: StoreModel) => item.store_id === id);
  }

  public getFilterList(payload: string): void {
    this.filterList = [];
    if (payload) {
      this.itemList.filter((store: StoreModel) => store.store_name.indexOf(payload) > -1);
    }
  }
}
