import { Injectable } from '@angular/core';
import { Observable, Subscriber } from 'rxjs';
import { CardModel } from 'src/app/core/models/cards.model';
import { map } from 'lodash';
import { HttpService } from './http/http.service';
import { EventMessageModel } from 'src/app/core/models/event-message.model';
import { EventMessage } from 'src/app/core/classes/EventMessage';

@Injectable({
  providedIn: 'root',
})
export class CardService {
  public cardsList: CardModel[];
  constructor() {
    this.cardsList = [];
  }

  public getCard<T>(itemList: T[], formatFunc: (data: T) => CardModel): Observable<EventMessageModel<CardModel[]>> {
    return new Observable<EventMessageModel<CardModel[]>>((sub: Subscriber<EventMessageModel<CardModel[]>>) => {
      const res: CardModel[] = map(itemList, (item: T) => {
        return formatFunc(item);
      });

      if (res.length > 0) {
        sub.next(new EventMessage(true, '', res));
        this.cardsList = res;
        sub.complete();
      }
      sub.error(new EventMessage(false, 'No data from server or data format failed', res));
      sub.complete();
    });
  }

  // private deserialize(item: CardModel) {
  //   const cards = [];
  //   const keys = Object.keys(item);

  //   for (const type of keys) {
  //     const data: ProductModel = {
  //       product_id: '',
  //       price: 0,
  //       // count: 0,
  //       // remark: '',
  //       // plus_item: [],
  //     };
  //     const target = get(item, type, {});
  //     const title = target.title;
  //     const items = target.product;

  //     const arr = [];
  //     for (const x of items) {
  //       if (x) {
  //         const card = {
  //           id: x.id,
  //           img: x.img,
  //           title: x.title,
  //           promo: x.promo,
  //           locate: x.locate,
  //           time: x.locate,
  //           redirectTo: `pages/products/${x.id}`,
  //         };
  //         arr.push(card);
  //       }
  //     }
  //     // data.type = type;
  //     // data.title = title;
  //     // data.card = arr;
  //     cards.push(data);
  //   }

  //   return cards;
  // }
}
