import { BehaviorSubject, EMPTY, from, Observable, of, Subject, Subscriber } from 'rxjs';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { concatMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { IAuthService, SignInPayLoad, SignUpPayLoad } from 'src/app/core/interface/IAuthService';
import { IUser } from 'src/app/core/models/user.modal';
import { JWTOptions } from '../classes/AppModuleOption';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AuthModel, DirtyAuth } from '../models/auth.model';
import { Auth } from '../classes/Auth';
import { ApiResponse } from '../models/api';
import { HttpService } from './http/http.service';
import { StorageType } from '../models/storage.model';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class AuthService implements IAuthService {
  protected token: string;
  private authenticateTimeout: number;
  private account: BehaviorSubject<AuthModel>;
  public account$: Observable<AuthModel>;
  private _account: AuthModel;
  public get accountValue(): AuthModel {
    return this._account;
  }
  constructor(
    public JWT: JWTOptions,
    private storageService: StorageService,
    private router: Router,
    private http: HttpService,
  ) {
    this.token = JWT.key;
    this.storageService.set(StorageType.local, this.token, JWT.key);
    this._account = new Auth();
    this.account = new BehaviorSubject<AuthModel>(this._account);
    this.account$ = this.account.asObservable();
    this.authenticateTimeout = 0;
  }

  public getToken(): string {
    return this.storageService.get(StorageType.local, this.token);
  }

  public logIn(payload: SignInPayLoad): void {
    this.http
      .post<DirtyAuth>('login', {
        body: payload,
      })
      .pipe(untilDestroyed(this))
      .subscribe((res: ApiResponse<DirtyAuth>) => {
        if (res.status) {
          this._account = new Auth(res.result);
          this.account.next(this._account);
        }
      });
  }

  public fbLogin(): void {
    this.facebookLogin()
      .pipe(concatMap(accessToken => this.apiAuthenticate(accessToken)))
      .subscribe(res => {
        console.log('facebook token', res);
      });
  }

  private facebookLogin() {
    // login with facebook and return observable with fb access token on success
    return from(
      new Promise<fb.StatusResponse>(resolve => {
        FB.login(resolve);
      }),
    ).pipe(
      concatMap(({ authResponse }) => {
        if (!authResponse) return EMPTY;
        return of(authResponse.accessToken);
      }),
    );
  }

  public fbLogout(): void {
    // revoke app permissions to logout completely because FB.logout() doesn't remove FB cookie
    FB.api('/me/permissions', 'delete', {}, () => FB.logout());
    this.stopAuthenticateTimer();
    this.logout();
    this.router.navigate(['/']);
  }

  public apiAuthenticate(accessToken: string): Observable<ApiResponse<AuthModel>> {
    // authenticate with the api using a facebook access token,
    // on success the api returns an account object with a JWT auth token
    return this.http.post<AuthModel>('authenticate', { accessToken }).pipe(untilDestroyed(this));
  }

  private startAuthenticateTimer(): void {
    console.log('BE response token', this.accountValue.token);
    // parse json object from base64 encoded jwt token
    // const jwtToken = parseJson(atob(this.accountValue.token.split('.')[1]))

    // // set a timeout to re-authenticate with the api one minute before the token expires
    // const expires = new Date(jwtToken.exp * 1000)
    // const timeout = expires.getTime() - Date.now() - 60 * 1000
    // const { accessToken } = FB.getAuthResponse()
    // this.authenticateTimeout = setTimeout(() => {
    //   this.apiAuthenticate(accessToken).subscribe()
    // }, timeout)
  }

  private stopAuthenticateTimer(): void {
    // cancel timer for re-authenticating with the api
    clearTimeout(this.authenticateTimeout);
  }

  public guestToken(): void {
    // return this.http.post<AuthModel>('guest', {}).subscribe();
  }

  public signUp(payload: SignUpPayLoad): Observable<ApiResponse<IUser>> {
    return this.http.post<IUser>('register', {
      body: payload,
    });
  }

  public logout(): void {
    this._account = new Auth();
    this.account.next(this._account);
  }
}
