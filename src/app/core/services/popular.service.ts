import { Injectable } from '@angular/core';
import { IPopularService, PopularReq } from 'src/app/core/interface/IPopularService';
import { Observable, Subscriber } from 'rxjs';
import { HttpReq } from '../classes/HttpReq';
import { Popular } from '../classes/Popular';
import { DirtyPopular, PopularModel } from '../models/popular.model';
import { getString } from '../utils/lodash.function';
import { HttpService } from './http/http.service';
import { AuthService } from './auth.service';
import { ApiResponse } from '../models/api';
import { mockPopular } from 'src/mocks/popular';
import { AppService } from './app.service';

@Injectable({
  providedIn: 'root',
})
export class PopularService implements IPopularService {
  public itemList: PopularModel[];

  constructor(private http: HttpService, private auth: AuthService, private app: AppService) {
    this.itemList = [];
    this.http.setDefaultResData({ url: 'https://dev.justkitchen.com/api/public/brands/popular', data: mockPopular });
  }

  public fetchData(): Observable<PopularModel[]> {
    return new Observable<PopularModel[]>((sub: Subscriber<PopularModel[]>) => {
      const body: PopularReq = {
        token: getString(this.auth, 'token', ''),
      };
      const req: HttpReq = new HttpReq({ body });
      this.http
        .post<DirtyPopular[]>('https://dev.justkitchen.com/api/public/brands/popular', req)
        .subscribe((res: ApiResponse<DirtyPopular[]>) => {
          if (res.status) {
            this.itemList = res.result.map((item: DirtyPopular) => new Popular(item));
            sub.next(this.itemList);
          } else {
            this.app.errorEvent$.next(res.message);
            sub.error([]);
          }
          sub.complete();
        });
    });
  }
}
