import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { Injectable } from '@angular/core';
import { Observable, Subscriber, BehaviorSubject } from 'rxjs';
import { ICartService } from 'src/app/core/interface/ICartService';
import { CartProductModel, ProductModel } from 'src/app/core/models/product.model';
import { HttpService } from './http/http.service';
import { ProductsService } from './products.service';
import { EventMessage } from 'src/app/core/classes/EventMessage';
import { StoreService } from './store.service';
import { StoreModel } from '../models/store.model';
import { Store } from '../classes/Store';
import { StorageService } from './storage.service';
import parseJson from 'parse-json';
import { StorageType } from '../models/storage.model';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class CartService implements ICartService {
  private currentStore: StoreModel;
  public itemList: CartProductModel[];
  private countSub: BehaviorSubject<number>;
  public countSub$: Observable<number>;
  public addressComponent: google.maps.GeocoderAddressComponent[];

  public get storeInfo(): StoreModel {
    return this.currentStore;
  }
  constructor(
    private http: HttpService,
    private product: ProductsService,
    private store: StoreService,
    private storage: StorageService,
  ) {
    const _storeStr: string = this.storage.get(StorageType.local, 'store');
    this.currentStore = _storeStr !== '' ? parseJson(_storeStr) : new Store();

    this.itemList = parseJson(this.storage.get(StorageType.local, 'cart')) || [];
    this.countSub = new BehaviorSubject(this.itemList.length);
    this.countSub$ = this.countSub.asObservable();
    this.addressComponent = [];
    if (this.itemList.length === 0) {
      this.countSub = new BehaviorSubject(0);
      return;
    }
    this.store.changeEvent$.pipe(untilDestroyed(this)).subscribe((res: StoreModel[]) => {
      if (res.length > 0) {
        const _store = this.store.getStoreById(this.itemList[0].product.store_id) || new Store();
        this.currentStore = _store;
      }
    });
  }

  public create(item: CartProductModel): Observable<EventMessage<CartProductModel[]>> {
    return new Observable<EventMessage<CartProductModel[]>>((sub: Subscriber<EventMessage<CartProductModel[]>>) => {
      if (this.itemList.length > 0) {
        const check = this.handleCheckEnable(item.product.store_id);
        if (check !== '') {
          sub.error(new EventMessage(false, check, this.itemList));
          sub.complete();
        }
      } else {
        this.currentStore = this.store.getStoreById(item.product.store_id) || new Store();
        this.storage.set(StorageType.local, 'store', JSON.stringify(this.currentStore));
      }
      this.itemList.push(item);
      this.countSub.next(this.itemList.length);
      this.storage.set(StorageType.local, 'cart', JSON.stringify(this.itemList));
      sub.next(new EventMessage(true, '', this.itemList));
    });
  }

  public modify(item: CartProductModel, count: number, mark: string): Observable<EventMessage<CartProductModel[]>> {
    return new Observable<EventMessage<CartProductModel[]>>((sub: Subscriber<EventMessage<CartProductModel[]>>) => {
      const index: number = this.itemList.findIndex(
        (res: CartProductModel) => item.product.product_id === res.product.product_id,
      );
      if (index < 0) {
        sub.error(new EventMessage(false, 'This product is not in cart', this.itemList));
        sub.complete();
      }

      this.itemList.splice(index, 1);
      if (count > -1) {
        this.itemList.push(item);
      }
      if (this.itemList.length === 0) {
        this.currentStore = new Store();
        this.storage.set(StorageType.local, 'store', JSON.stringify(''));
      }
      this.storage.set(StorageType.local, 'cart', JSON.stringify(this.itemList));
      sub.next(new EventMessage(true, '', this.itemList));
    });
  }

  public clean(): void {
    this.currentStore = new Store();
    this.itemList = [];
    this.storage.set(StorageType.local, 'store', JSON.stringify(''));
    this.storage.set(StorageType.local, 'cart', JSON.stringify(this.itemList));
  }

  // public handleOrder(): Observable<EventMessage<PreOrderItemModel[]>> {
  //   return new Observable<EventMessage<PreOrderItemModel[]>>((sub: Subscriber<EventMessage<PreOrderItemModel[]>>) => {
  //     if (this.itemList.length === 0) {
  //       sub.error(new EventMessage(false, 'Empty cart'));
  //     }
  //     const res: PreOrderItemModel[] = this.itemList.map(
  //       (item: CartModel) => new PreOrderItem(item.cartItem.product_id, item.cartItem.price, item.count, item.remark),
  //     );

  //     sub.next(new EventMessage(true, '', res));
  //     sub.complete();
  //   });
  // }

  private handleCheckEnable(id: string): string {
    if (this.currentStore.store_id === '' || id === '') {
      return 'Empty Store info please contact developer';
    }
    if (this.currentStore.store_id !== id) {
      return 'Different Store';
    }
    return '';
  }
}
