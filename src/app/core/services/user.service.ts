import { ErrorMsgEvent } from './../models/event-message.model';
import { Injectable } from '@angular/core';
import { CustomerModel } from '../models/user.modal';
import { BehaviorSubject, take, takeUntil, Observable, Subscriber } from 'rxjs';
import { User } from '../classes/User';
import { HttpService } from './http/http.service';
import { ApiResponse } from '../models/api';
import { isArray, takeWhile } from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private _user: CustomerModel;
  private userEvent: BehaviorSubject<CustomerModel>;
  public userEvent$: Observable<CustomerModel>;

  public get user(): CustomerModel {
    return this._user;
  }

  constructor(private http: HttpService) {
    this._user = new User();
    this.userEvent = new BehaviorSubject(this._user);
    this.userEvent$ = this.userEvent.asObservable();
  }

  public fetchData(customer_no: string): Observable<CustomerModel> {
    return new Observable<CustomerModel>((subscriber: Subscriber<CustomerModel>) => {
      const url: string = 'https://apidev.justkitchen.com/api/customer/' + customer_no;
      this.http.get<ApiResponse<unknown>>(url).subscribe((res: ApiResponse<unknown>) => {
        console.log(res);
        if (res.status && isArray(res.result) && res.result.length > 0) {
          const tmp: CustomerModel = new User(res.result[0]);
          subscriber.next(tmp);
          this._user = tmp;
          this.userEvent.next(tmp);
        }
        subscriber.complete();
      });
    });
  }
}
