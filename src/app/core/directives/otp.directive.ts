import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'input[otpInput]',
})
export class OtpDirective {
  constructor(private el: ElementRef) {}

  @HostListener('keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    if (event.key == 'Backspace' && !this.el.nativeElement.value) {
      if (this.el.nativeElement?.previousElementSibling) {
        this.el.nativeElement.previousElementSibling.focus();
      }
    }

    if (event.key === 'ArrowLeft') {
      if (this.el.nativeElement?.previousElementSibling) {
        this.el.nativeElement.previousElementSibling.focus();
        setTimeout(() => this.el.nativeElement.previousElementSibling.setSelectionRange(1, 1));
      } else {
        setTimeout(() => this.el.nativeElement.setSelectionRange(1, 1));
      }
    }

    if (event.key === 'ArrowRight') {
      if (this.el.nativeElement?.nextElementSibling) {
        this.el.nativeElement.nextElementSibling.focus();
        setTimeout(() => this.el.nativeElement.nextElementSibling.setSelectionRange(1, 1));
      }
    }
  }

  @HostListener('input', ['$event']) onInputChange(event: InputEvent) {
    const initalValue = this.el.nativeElement.value;
    const isDeleteType = event.inputType === 'deleteContentBackward' || event.inputType === 'deleteContentForward';
    // limit number only
    this.el.nativeElement.value = initalValue.replace(/[^0-9]*/g, '');
    if (initalValue !== this.el.nativeElement.value) {
      event.stopPropagation();
      return;
    }
    // do something
    if (!isDeleteType && this.el.nativeElement?.nextElementSibling) {
      this.el.nativeElement.nextElementSibling.focus();
    }
  }
}
