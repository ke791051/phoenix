import { Directive, Input, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[blockInitAnimate]',
})
export class BlockInitAnimateDirective {
  @Input() blockTime = 800;
  constructor(private element: ElementRef, private renderer: Renderer2) {
    const el = this.element.nativeElement;
    this.renderer.addClass(el, 'block-init-animate-directive');

    setTimeout(() => {
      this.renderer.removeClass(el, 'block-init-animate-directive');
    }, this.blockTime);
  }
}
