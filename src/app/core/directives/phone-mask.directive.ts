import { Directive, ElementRef, HostListener, Injector, Input, Renderer2 } from '@angular/core';
import { ControlContainer, NgControl } from '@angular/forms';
import { get } from 'lodash';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: '[formControlName][phoneMask]',
})
export class PhoneMaskDirective {
  @Input() formControlName!: string;
  @Input() maxLength!: number;
  constructor(
    private injector: Injector,
    private el: ElementRef,
    private renderer: Renderer2,
    public ngControl: NgControl,
  ) {
    this.formControlName = '';
    this.maxLength = 15;
  }

  get control() {
    const target = get(this.controlContainer, `control.${this.formControlName}`);
    return target;
  }

  get controlContainer() {
    return this.injector.get(ControlContainer);
  }

  ngOnInit() {
    this.renderer.setAttribute(this.el.nativeElement, 'maxLength', this.maxLength.toString());
  }

  @HostListener('keydown.backspace', ['$event'])
  keyBackspace(e: Event): void {
    const val: string = get(e.target, 'value');
    this.onInputChange(val);
  }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(e: Event): void {
    const val: string = get(e.target, 'value');
    this.onInputChange(val);
  }

  onInputChange(val: string): void {
    let newVal = val.replace(/\D/g, '');

    // validate if reach end
    if (newVal.length === 9) {
      this.control.markAsTouched();
    }

    // start mask
    if (newVal.length === 0) {
      newVal = '';
    } else if (newVal.length <= 3) {
      newVal = newVal.replace(/^(\d{0,3})/, '$1');
    } else if (newVal.length <= 6) {
      newVal = newVal.replace(/^(\d{0,3})(\d{0,3})/, '$1 - $2');
    } else if (newVal.length <= 9) {
      newVal = newVal.replace(/^(\d{0,3})(\d{0,3})(\d{0,3})/, '$1 - $2 - $3');
    } else {
      newVal = newVal.substring(0, 9);
      newVal = newVal.replace(/^(\d{0,3})(\d{0,3})(\d{0,3})/, '$1 - $2 - $3');
    }
    if (this.ngControl.valueAccessor !== null) this.ngControl.valueAccessor.writeValue(newVal);
  }
}
