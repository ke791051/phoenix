import { Directive, HostListener, Input, ElementRef, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: 'img[appImageFallback]',
})
export class ImageFallbackDirective {
  @Input() default?: string;
  @Input() placehoder?: string;
  @Output() isComplete: EventEmitter<boolean>;
  // @HostBinding('src') src?: string;

  constructor(private ref: ElementRef) {
    this.isComplete = new EventEmitter<boolean>();
  }

  @HostListener('error')
  public handleImageFallBack(): void {
    const defaultImgSrc: string = this.default || '';
    const element: HTMLImageElement = <HTMLImageElement>this.ref.nativeElement;
    element.src = defaultImgSrc;
    this.isComplete.emit(false);
  }
}
