import { ChangeDetectorRef, Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';
import { ScrollbarEvents } from 'swiper/types';

// eslint-disable-next-line @angular-eslint/directive-selector
@Directive({ selector: '[checkScrolling]' })
export class CheckScrollingDirective {
  @Output() isScrollToBottom = new EventEmitter<any>();
  constructor(private el: ElementRef, private cdrf: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    const scrollHeight = this.el.nativeElement.scrollHeight;
    const Height = this.el.nativeElement.clientHeight;
    // no scrollbar
    if (scrollHeight <= Height) {
      this.isScrollToBottom.emit(true);
      this.cdrf.detectChanges();
    }
  }

  @HostListener('scroll', ['$event'])
  onScroll(_event: ScrollbarEvents) {
    const scrollHeight = this.el.nativeElement.scrollHeight;
    const scrollTop = this.el.nativeElement.scrollTop;
    const offsetHeight = this.el.nativeElement.offsetHeight;

    if (scrollHeight === Math.ceil(scrollTop + offsetHeight)) {
      this.isScrollToBottom.emit(true);
    }
  }
}
