import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/core/shared.module';
import { RouterModule } from '@angular/router';
import { EnvServiceProvider } from './services/env.service';
import { TranslocoRootModule } from 'src/app/transloco-root.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule, RouterModule, TranslocoRootModule],
  providers: [EnvServiceProvider],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: CoreModule,
      // Singleton services
      providers: [],
    };
  }
}
