import { Pipe, PipeTransform } from '@angular/core'
@Pipe({
  name: 'amountFilter',
})
export class AmountFilterPipe implements PipeTransform {
  transform(item: any[], count?: number): any {
    if (!count) {
      return item
    } else {
      return item.filter((_res, idx) => {
        return idx < count
      })
    }
  }
}
