import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'productOptionPrice',
})
export class ProductOptionPricePipe implements PipeTransform {
  public transform(num: number): string {
    if (num === 0) return '';

    return `+$${num}`;
  }
}
