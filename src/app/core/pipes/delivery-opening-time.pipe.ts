import { Pipe, PipeTransform } from '@angular/core';
import { get } from 'lodash';
import { Week, Section } from '../models/store.model';

@Pipe({
  name: 'deliveryOpeningTime',
})
export class DeliveryOpeningTimePipe implements PipeTransform {
  public transform(weekList: Array<Week>): string {
    const today = new Date().getDay() - 1;
    const weekMapList: string[] = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    const weekMap: string = weekMapList[today];
    const weekData: Week | undefined = weekList.find((item: Week) => item.day === weekMap);
    if (!!weekData) {
      if (!weekData.isOpen) return '本日公休';
    }
    const section: Section = get(weekData, 'section', { open: '', close: '' });
    const opening: string = this.parseNoon(section.open);
    const closing: string = this.parseNoon(section.close);
    return `${opening} - ${closing}`;
  }
  private parseNoon(timeString: string): string {
    if (timeString === '') return '';
    const hours: string = timeString.slice(0, 2);
    const minute: string = timeString.slice(2, 2);
    const regex: RegExp = /^[0-9\s]*$/;
    let tmp: string = hours.replace(regex, '');
    const num = parseInt(tmp, 10);
    const fixNum: number = num > 12 ? num - 12 : num;
    const fixNumString: string = fixNum.toString().padStart(2, '0');
    const noonString: string = num >= 12 ? 'PM' : 'AM';
    return `${fixNumString}:${minute}${noonString}`;
  }
}
