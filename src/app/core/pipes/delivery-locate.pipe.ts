import { Pipe, PipeTransform } from '@angular/core';
import { isString } from 'lodash';

@Pipe({
  name: 'deliveryLocate',
})
export class DeliveryLocatePipe implements PipeTransform {
  transform(num: number | string): string {
    if (num === '0' || num === 0) return '0';
    let tmp: number = 0;
    if (isString(num)) {
      const regex: RegExp = /^[0-9\s]*$/;
      let numString: string = num.replace(regex, '');
      if (numString === '') numString = '0';
      tmp = parseInt(numString, 10);
    }
    const km: number = tmp / 1000 + 0.1;
    const res: string = km.toFixed(1).toString();
    return `${res}`;
  }
}
