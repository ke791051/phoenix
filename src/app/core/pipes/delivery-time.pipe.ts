import { Pipe, PipeTransform } from '@angular/core';
import { isString } from 'lodash';
import { DeliveryTime } from '../enums/delivery.enum';

@Pipe({
  name: 'deliveryTime',
})
export class DeliveryTimePipe implements PipeTransform {
  public transform(num: number | string): string {
    if (num === '0' || num === 0) return '0-10';
    if (isString(num)) {
      const regex: RegExp = /^[0-9\s]*$/;
      let tmp: string = num.replace(regex, '');
      if (tmp === '') tmp = '0';
      const newNum: number = parseInt(tmp, 10);
      return `${newNum - 5}-${newNum + 5}`;
    }
    return `${num - 5}-${num + 5}`;
  }
}
