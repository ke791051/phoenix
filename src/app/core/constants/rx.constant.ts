export const RxConstant = {
  handleRestAPIError: {
    cmd: 'handleRestAPIError',
  },
  handleRestAPISuccess: {
    cmd: 'handleRestAPISuccess',
  },
}
