import { Component, HostListener } from '@angular/core'

@Component({
  template: '',
})
export abstract class ComponentCanDeactivate {
  abstract canDeactivate(): boolean

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (!this.canDeactivate()) {
      // Note: we cant custom text here cause latest browsers not recommended and have taken down support
      $event.returnValue = true
    }
  }
}
