import { get, isString, isNumber } from 'lodash';

export function getString(obj: unknown, path: string, defaultValue: string): string {
  const res: string = get(obj, path);
  if (!isString(res) || res === '') {
    return defaultValue;
  }
  return res;
}

export function getNumber(obj: unknown, path: string, defaultValue: number): number {
  const res: number = get(obj, path);
  if (!isNumber(res) || !res) {
    return defaultValue;
  }
  return res;
}
