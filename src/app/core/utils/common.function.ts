import { forEach, set, get } from 'lodash';
import { dataType } from '../../types/data.type';
// export const __cloneDeep = (inObject: unknown[]) => {
//   let outObject: any, value, key;

//   if (typeof inObject !== 'object' || inObject === null) {
//     return inObject; // Return the value if inObject is not an object
//   }

//   // Create an array or object to hold the values
//   // eslint-disable-next-line prefer-const
//   outObject = Array.isArray(inObject) ? [] : {};

//   for (key in inObject) {
//     value = inObject[key];

//     // Recursively (deep) copy for nested objects, including arrays
//     outObject[key] = __cloneDeep(value);
//   }

//   return outObject;
// };

// export const __isEmptyObj = (obj: any) => {
//   for (const key in obj) {
//     if (Object.prototype.hasOwnProperty.call(obj, key)) {
//       return false;
//     }
//   }
//   return true;
// };

// // Spread object into an array {1:{key:x},2:{key:y}} => [{key:x},{key:y}]
// export const __spreadObj = (obj: any) => {
//   const newObj = Object.keys(obj).map(key => {
//     return obj[key];
//   });
//   return newObj;
// };

// //  [{a:1, b:2},{c:3, d:4},{e:5, f:6}] => {a:1, b:2, c:3, d:4, e:5, f:6}
export const __mergeArrayObj = (arrObj: any) => {
  return arrObj.reduce(function (acc: any, x: any) {
    for (const key in x) {
      set(acc, key, get(x, key));
    }
    return acc;
  }, {});
};

// { 'ab.cd.e' : 'foo', 'ab.cd.f' : 'bar','ab.g' : 'foo2'} => {ab: {cd: {e:'foo', f:'bar'}, g:'foo2'}}
export const __deepDotObj = (obj: dataType) => {
  let result: dataType = {};

  forEach(obj, (path: string) => {
    const parts: string[] = path.split('.');

    // Create sub-objects along path as needed
    let target = result;
    while (parts.length > 1) {
      const part = parts.shift() || '';
      target = set(target, part, set(target, part, {}));
    }

    // Set value at end of path
    set(target, '0', get(obj, path));
  });

  return result;
};
