import { LangInfo } from 'src/app/types/commonType';
import { Language } from 'src/app/core/enums/language.enum';

export const LangOrder: LangInfo[] = [
  {
    name: Language.TW,
    order: '1',
    display: '繁體中文-台灣',
    alias: '繁體中文',
  },
  {
    name: Language.EN,
    order: '2',
    display: '英文',
    alias: 'English',
  },
];
