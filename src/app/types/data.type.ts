export type dataObject = Record<string, any>

export type dataList = dataObject[]

export type dataType = dataObject | dataList
