export type GoogleSheetApi = {
  majorDimension: string;
  range: string;
  values: Array<Array<string>>;
};
