export type JsonObject<T> = {
  [key: string]: T;
};

export type LangInfo = {
  name: string;
  order: string;
  display: string;
  alias: string;
};

export type Location = {
  latitude: number;
  longitude: number;
};

export type HttpParamsModel = { [param: string]: string };

export type InputProp = {
  buttonText?: string;
  placeholder?: string;
  headerTitle?: string;
  label?: string;
  infoContent?: string;
  formTypeValue?: string;
  mode?: string;
  content?: string;
  confirmButtonText?: string;
  isConfirmMode?: boolean;
  buttonTextUpdate?: string;
  buttonTextDel?: string;
};

export type imgTextList = {
  src: string;
  title: string;
  subtTitle?: string;
  content: string;
  redirectTo: string;
};
