import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AddressComponent } from '@app/components/modal/address/address.component';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/core/services/auth.service';
import { AppService } from '../../core/services/app.service';
import { StoreService } from '../../core/services/store.service';
import { CartService } from '../../core/services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() platForm!: number;
  public isAuthenticated: boolean;
  public errorMessage = null;
  public addressValue: string;
  public currentTime: string;
  public timeList: string[];

  constructor(
    public app: AppService,
    private Auth: AuthService,
    private cdrf: ChangeDetectorRef,
    private modal: NgbModal,
    private store: StoreService,
    public cart: CartService,
    private router: Router,
  ) {
    this.isAuthenticated = false;
    this.addressValue = '';
    // this.Auth.account$.subscribe((res: AuthModel) => {
    //   if (res.token !== '') {
    //     this.user = this.Auth.accountValue;
    //   }
    // });
    this.currentTime = '';
    this.timeList = ['現在', '稍後'];
  }
  ngOnInit(): void {
    this.currentTime = this.timeList[0];
    // this.user = this.Auth.accountValue;
  }

  public logOut(): void {
    this.Auth.logout();
  }

  public handleTimeChange($event: string) {
    this.currentTime = $event;
  }

  public typeSearch($event: any) {
    this.addressValue = $event?.vicinity || '-';
    this.cdrf.detectChanges();
  }

  public handleAddressClick(): void {
    const modalRef: NgbModalRef = this.modal.open(AddressComponent);
  }

  public handleSearchEvent(payload: string): void {
    this.store.getFilterList(payload);
  }

  public handleTypingEvent(payload: string): void {
    this.store.getFilterList(payload);
  }

  public handleCartBtnClick(): void {
    if (this.cart.itemList.length > 0) {
      this.router.navigateByUrl('/cart');
    }
  }
}
