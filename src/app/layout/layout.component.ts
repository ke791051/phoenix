import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreakpointsService } from 'src/app/core/services/breakpoints.service';
import { filter, map } from 'rxjs/operators';
import { AppService } from 'src/app/core/services/app.service';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  // public isPlat$: Observable<number>;
  public platForm: number;
  public isNavbarShow: boolean;
  public isHome: boolean;

  constructor(public app: AppService, private breakpoints: BreakpointsService, private route: ActivatedRoute) {
    // this.isPlat$ = new Observable<number>();
    this.platForm = 0;
    this.isNavbarShow = false;
    this.isHome = false;
  }

  ngOnInit(): void {
    this.breakpoints
      .subscribeToLayoutChanges<string>()
      .pipe(map((res: string) => this.breakpoints.setPlatform(res)))
      .subscribe(res => {
        this.platForm = res;
      });
    this.route.data.pipe(filter(data => data['noNav'])).subscribe(data => (this.isNavbarShow = data['noNav']));
    this.route.url.subscribe(url => {
      this.isHome = url.length === 0;
    });
  }

  public handleNavToggle(newStatus: boolean): void {
    this.app.toggleNavbarEvent(newStatus);
  }
}
