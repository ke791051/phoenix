import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { CardModel } from 'src/app/core/models/cards.model';
import { TYPES } from 'src/app/core/models/types.model';

@Component({
  selector: 'app-card-widget',
  templateUrl: './card-widget.component.html',
  styleUrls: ['./card-widget.component.scss'],
})
export class CardWidgetComponent implements OnInit {
  @Input() types!: TYPES;
  @Input() title!: string;
  @Input() itemList?: CardModel[];
  constructor(private router: Router) {}

  ngOnInit(): void {
    console.log();
  }
}
