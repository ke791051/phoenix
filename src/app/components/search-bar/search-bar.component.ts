import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject, Observable, throttleTime, BehaviorSubject } from 'rxjs';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { StorageService } from '../../core/services/storage.service';
import { StorageType } from '@app/core/models/storage.model';

@UntilDestroy()
@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  @Input() placeholder!: string;
  @Input() historySearch!: string[];
  @Output() typingEvent: EventEmitter<string>;
  @Output() searchEvent: EventEmitter<string>;
  private value: string;
  private valueSub: BehaviorSubject<string>;
  public valueSub$: Observable<string>;
  constructor(private storage: StorageService) {
    this.value = '';
    this.valueSub = new BehaviorSubject('');
    this.valueSub$ = this.valueSub.asObservable();
    this.typingEvent = new EventEmitter();
    this.searchEvent = new EventEmitter();
  }

  ngOnInit(): void {
    this.valueSub$.pipe(untilDestroyed(this), throttleTime(1000)).subscribe((res: string) => {
      this.typingEvent.emit(res);
    });
  }

  public handleSubmitEvent(): void {
    let newHistorySearch: string[] = this.historySearch;
    const tmpIndex: number = newHistorySearch.indexOf(this.value);
    if (tmpIndex > -1) {
      newHistorySearch = newHistorySearch.splice(tmpIndex, 1);
    }
    newHistorySearch.push(this.value);
    this.storage.set(StorageType.local, 'search-history', JSON.stringify(newHistorySearch));
    this.searchEvent.emit(this.value);
  }

  public handleInputSearch(event: Event): void {
    const target: string = (event.target as HTMLInputElement).value;
    this.value = target;
    this.valueSub.next(target);
  }
}
