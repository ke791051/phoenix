import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-modal-otp',
  templateUrl: './ngbd-modal-otp.component.html',
  styleUrls: ['./ngbd-modal-otp.component.scss'],
})
export class NgbdModalOtpComponent implements OnInit {
  @Input() headerTitle!: string;
  @Input() phoneNumber!: string;
  @Input() buttonText!: string;
  @Input() verifyContent!: string;
  @Input() formType!: string;
  public isDisabled: boolean;
  public otpLength = 4;
  public timer = 60;
  public otpForm!: FormGroup;
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder) {
    this.headerTitle = '';
    this.phoneNumber = 'XXXXX';
    this.buttonText = '';
    this.verifyContent = '';
    this.isDisabled = true;
    this.otpLength = 4;
    this.timer = 60;
  }

  ngOnInit(): void {
    this.otpForm = this.fb.group({
      otp0: ['', Validators.required],
      otp1: ['', Validators.required],
      otp2: ['', Validators.required],
      otp3: ['', Validators.required],
    });
  }

  onInput() {
    if (this.otpForm.status === 'VALID') {
      this.isDisabled = false;
    } else {
      this.isDisabled = true;
    }
  }
}
