import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Component, Input, OnInit } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalService } from '../../core/services/modal.service';
import { ModalVisibleModel, ModalOptionModel } from '../../core/models/modal.model';
import { debounceTime, filter, map } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  providers: [ModalComponent, NgbModal],
})
export class ModalComponent implements OnInit {
  private _ref?: NgbModalRef;

  constructor(
    private config: NgbModalConfig,
    private ngModal: NgbModal,
    public modal: ModalService,
    private activeModal: NgbActiveModal,
  ) {
    // config.backdrop = 'static';
    config.scrollable = true;
    config.centered = true;
    config.backdrop = true;
    config.size = 'lg';
  }

  ngOnInit(): void {
    this.modal.visibleSub$
      .pipe(
        debounceTime(300),
        filter((res: ModalVisibleModel) => res.visible),
      )
      .subscribe((res: ModalVisibleModel) => {
        if (res.visible) {
          this.open(res);
        } else {
          this.activeModal.dismiss('Cross click');
        }
      });
    this.modal.optionSub$.pipe(untilDestroyed(this)).subscribe((res: ModalOptionModel) => {
      Object.assign(this.config, res);
    });
  }

  private open(payload: ModalVisibleModel): void {
    if (payload.visible) {
      const _ref: NgbModalRef = this.ngModal.open(payload.content, { windowClass: 'modal-dialog-centered' });
      // _ref.componentInstance.product = payload.instances.product;
      _ref.shown.subscribe(() => {
        // modal 完成顯示動畫後
      });
      _ref.dismissed.subscribe(() => {
        // modal 執行隱藏動畫
        this.modal.visibleSub$.next({ visible: false });
      });
      _ref.hidden.subscribe(() => {
        // modal 完成隱藏動畫後
      });

      Object.assign(_ref.componentInstance, payload.instances);
      return;
    }
    this.activeModal.dismiss('Cross click');
  }

  // public hide() {
  //   NgbModalRef.
  // }
}
