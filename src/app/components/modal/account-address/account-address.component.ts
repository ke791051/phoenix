import { Component, ElementRef, Input, NgZone, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AlertComponent } from '@app/components/alert/alert.component';
import { dataType } from '@app/types/data.type';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-account-address',
  templateUrl: './account-address.component.html',
  styleUrls: ['./account-address.component.scss'],
})
export class AccountAddressComponent implements OnInit {
  public placeholder: string;
  public isSettingMode: boolean;
  public isDisabled: boolean;
  public currentHandOver: string;

  @Input() headerTitle!: string;
  @Input() showClosed!: boolean;
  @Input() mode!: string;

  // @ViewChild('search')
  // public searchElementRef: ElementRef;

  constructor(
    private ngZone: NgZone,
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private renderer2: Renderer2,
  ) {
    console.log('modalService :>> ', this.modalService);
    this.placeholder = '請輸入姓名';
    this.isSettingMode = false;
    this.isDisabled = true;
    this.currentHandOver = '1';
  }

  ngOnInit(): void {
    const result = this.mode;
    if (result === 'Setting') {
      this.isSettingMode = false;
    } else if (result === 'isSetting') {
      this.isSettingMode = true;
    } else {
      this.isSettingMode = true;
    }
  }

  public toggleSettingMode(status: boolean): void {
    this.isSettingMode = status;
  }

  public openCancelModal(): void {
    const modalRef = this.modalService.open(AlertComponent, { animation: false, windowClass: 'logout-modal' });

    const input: dataType = {
      content: '確定要捨棄編輯中的內容嗎?',
      confirmButtonText: '確定',
      isConfirmMode: false,
    };

    Object.assign(modalRef.componentInstance, input);
  }

  public handleHandOver(target: string): void {
    this.currentHandOver = target;
  }
}
