import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { AlertComponent } from '@app/components/alert/alert.component';
import { dataType } from '@app/types/data.type';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
import { GoogleMapService } from '../../../core/services/google-map.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
})
export class AddressComponent implements OnInit {
  public placeholder: string;
  public isSettingMode: boolean;
  public isDisabled: boolean;
  public currentHandOver: string;
  public options: Options;
  public formattedAddress: string;

  @Input() headerTitle!: string;
  @Input() showClosed!: boolean;
  @Input() mode!: string;

  @ViewChild('placesRef') placesRef!: GooglePlaceDirective;
  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal, public gms: GoogleMapService) {
    this.placeholder = '請輸入地址';
    this.isSettingMode = false;
    this.isDisabled = true;
    this.currentHandOver = '1';
    this.options = new Options({
      componentRestrictions: {
        country: 'TW',
      },
    });
    this.formattedAddress = '';
  }

  ngOnInit(): void {
    const result = this.mode;
    if (result === 'Setting') {
      this.isSettingMode = false;
    } else if (result === 'isSetting') {
      this.isSettingMode = true;
    } else {
      this.isSettingMode = true;
    }
  }

  public toggleSettingMode(status: boolean): void {
    this.isSettingMode = status;
  }

  public openCancelModal(): void {
    const modalRef = this.modalService.open(AlertComponent, { animation: false, windowClass: 'logout-modal' });

    const input: dataType = {
      content: '確定要捨棄編輯中的內容嗎?',
      confirmButtonText: '確定',
      isConfirmMode: false,
    };

    Object.assign(modalRef.componentInstance, input);
  }

  public handleHandOver(target: string): void {
    this.currentHandOver = target;
  }

  public handleAddressChange($event: any): void {
    this.formattedAddress = $event.formatted_address;
    console.log($event);
  }
}
