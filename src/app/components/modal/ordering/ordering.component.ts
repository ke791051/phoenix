import { Component, Input, OnInit } from '@angular/core';
import { OrderPayload } from '@app/core/classes/Order';
import { ModalVisibleModel } from '@app/core/models/modal.model';
import { CartProductModel } from '@app/core/models/product.model';
import { GoogleMapService } from '@app/core/services/google-map.service';
import { ModalService } from '@app/core/services/modal.service';
import { CartService } from '../../../core/services/cart.service';

@Component({
  selector: 'app-ordering',
  templateUrl: './ordering.component.html',
  styleUrls: ['./ordering.component.scss'],
})
export class OrderingComponent implements OnInit {
  @Input() orderInfo!: CartProductModel;
  constructor(public cart: CartService, public gms: GoogleMapService, public modal: ModalService) {}

  ngOnInit(): void {
    if (!this.orderInfo) {
    }
  }
  public handleCancel(): void {
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: OrderingComponent,
      instances: { orderInfo: new OrderPayload() },
    };
    this.modal.visibleSub$.next(modalVisible);
  }
}
