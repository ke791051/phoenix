import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { SVGITEMS, IMGITEMS } from 'src/app/core/models/items.model';
import { TYPES } from 'src/app/core/models/types.model';
import { getString } from 'src/app/core/utils/lodash.function';

@Component({
  selector: 'app-item-widget',
  templateUrl: './item-widget.component.html',
  styleUrls: ['./item-widget.component.scss'],
})
export class ItemWidgetComponent implements OnInit {
  @Input() types!: TYPES;
  @Input() title!: string;

  @Input() svgItems!: SVGITEMS[];
  @Input() imgItems!: IMGITEMS[];

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.svgItems = [];
    this.imgItems = [];
  }

  selectItem(item: IMGITEMS) {
    this.goPages(getString(item, 'redirectTo', ''));
  }

  goPages(url: string) {
    if (url) this.router.navigate([url]);
  }
}
