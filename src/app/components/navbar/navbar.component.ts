import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/services/app.service';
import { StorageService } from '../../core/services/storage.service';
import { CustomerModel } from '../../core/models/user.modal';
import { User } from '../../core/classes/User';
import parseJson from 'parse-json';
import { StorageType } from '@app/core/models/storage.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  public customerInfo: CustomerModel;
  constructor(public app: AppService, private router: Router, private storage: StorageService) {
    this.customerInfo = new User();
  }
  ngOnInit(): void {
    const _customer: string = this.storage.get(StorageType.local, 'customerInfo');
    this.customerInfo = _customer === '' ? new User() : parseJson(_customer);
    console.log('NaveBar work success');
  }

  public navigate(url: string): void {
    this.router.navigateByUrl(url);
  }
}
