import { Router } from '@angular/router';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';
import { SLIDES } from 'src/app/core/models/slides.model';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  @ViewChild(NgbCarousel, { static: true })
  carousel!: NgbCarousel;
  @Input() slides: SLIDES[] = [];

  constructor(private router: Router) {}

  ngOnInit(): void {
    console.log();
  }

  public swipe(e: string): void {
    if (e === 'swiperight') {
      this.carousel.prev();
    } else {
      this.carousel.next();
    }
  }

  public selectCarouselEvent(): void {
    this.goPage(this.carousel.activeId);
  }

  public goPage(page: string): void {
    if (page) this.router.navigate([page]);
  }
}
