import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalOtpComponent } from '../ngbd-modal-otp/ngbd-modal-otp.component';

@Component({
  selector: 'app-account-name',
  templateUrl: './single-field-modal.component.html',
  styleUrls: ['./single-field-modal.component.scss'],
})
export class SingleFieldModalComponent {
  @Input() headerTitle!: string;
  @Input() showClosed!: boolean;
  @Input() buttonText!: string;
  @Input() label!: string;
  @Input() placeholder!: string;
  @Input() inForContent!: string;
  @Input() formTypeValue!: string;
  @Input() _value?: string;
  public isDisabled: boolean;
  public phoneNumber: string;
  public verifyContent: string;
  public formType: string;
  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal) {
    console.log('single-filed-modal.component.ts: modalService :>> ', this.modalService);
    this.isDisabled = true;
    this.phoneNumber = '';
    this.verifyContent = '';
    this.formType = '';
  }

  public openOptModal(num: string, verifyValue: string) {
    const modalRef = this.modalService.open(NgbdModalOtpComponent, { animation: false });
    const input = {
      phoneNumber: num,
      buttonText: '確定',
      verifyContent: verifyValue,
      formType: this.formTypeValue,
    };
    Object.assign(modalRef.componentInstance, input);
  }

  public toBottom(event: boolean) {
    if (event === true) {
      this.isDisabled = false;
    }
  }
}
