import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: ['./promotion.component.scss'],
})
export class PromotionComponent {
  @Input() title!: string;
  @Input() status: 'error' | 'primary' | 'waring';
  @Input() content!: string;
  constructor() {
    this.status = 'primary';
  }
}
