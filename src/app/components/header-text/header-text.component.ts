import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-text',
  templateUrl: './header-text.component.html',
  styleUrls: ['./header-text.component.scss'],
})
export class HeaderTextComponent {
  @Input() leftText!: string;
  @Input() rightText!: string;
  @Input() redirectLeft!: string;
  @Input() redirectRight!: string;

  constructor(private router: Router) {}

  goPage(url?: string) {
    if (url) this.router.navigate([url]);
  }
}
