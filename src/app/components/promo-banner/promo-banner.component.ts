import { Router } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { AppService } from 'src/app/core/services/app.service';
import { DeviceModel } from 'src/app/core/models/device.model';

@Component({
  selector: 'app-promo-banner',
  templateUrl: './promo-banner.component.html',
  styleUrls: ['./promo-banner.component.scss'],
})
export class PromoBannerComponent implements OnInit {
  @Input() platform!: DeviceModel;
  @Input() svgSize = '13px';
  public showPromo: boolean;
  public promoText: string;
  public redirectTo: string;
  constructor(private router: Router, private appService: AppService) {
    this.showPromo = true;
    this.promoText = '';
    this.redirectTo = '';
  }

  ngOnInit(): void {
    this.appService.promoText$.subscribe(promo => {
      this.showPromo = promo.show;
      this.promoText = promo.txt || '';
      this.redirectTo = promo.redirectTo || '';
    });

    // example
    this.appService.handlePromoTextChange({
      show: true,
      txt: '慶祝清明連假，全館限時優惠！04/02- 04/04 輸入折扣碼[freedelivery] 免外送費！',
      redirectTo: 'pages/cart',
    });
  }

  clickPromo() {
    if (this.redirectTo) {
      this.router.navigate([this.redirectTo]);
      this.appService.handlePromoTextChange({
        show: false,
      });
    }
  }
}
