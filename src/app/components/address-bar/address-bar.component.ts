import { ModalService } from './../../core/services/modal.service';
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  Output,
  OnInit,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription } from 'rxjs';
import { GoogleMapService } from 'src/app/core/services/google-map.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ModalOptionModel, ModalVisibleModel } from '@app/core/models/modal.model';
import { AddressComponent } from '../modal/address/address.component';
import { dataObject } from '@app/types/data.type';
@UntilDestroy()
@Component({
  selector: 'app-address-bar',
  templateUrl: './address-bar.component.html',
  styleUrls: ['./address-bar.component.scss'],
})
export class AddressBarComponent implements OnInit, AfterViewInit {
  get addressText(): string {
    return this._addressText;
  }
  @Input() time: Array<string>;
  @Input() currentTime: string;
  @Output() timeEvent: EventEmitter<string>;
  @ViewChild('addressRef')
  public addressRef!: ElementRef;

  private _addressText: string;
  public placeholder: string;
  public hasLoaded: BehaviorSubject<boolean>;

  constructor(public gms: GoogleMapService, private ngZone: NgZone, private modal: ModalService) {
    this._addressText = '';
    this.time = [];
    this.currentTime = '';
    this.timeEvent = new EventEmitter<string>();
    this.placeholder = '';
    this.hasLoaded = new BehaviorSubject<boolean>(false);
    this.gms.getGeoCodeStorage();
  }

  ngOnInit(): void {
    const drop = this.currentTime || this.time[0];
    this.timeEvent.emit(drop);
    const modalOpt: dataObject = {
      size: 'md',
    };
    this.modal.optionSub$.next(modalOpt);
  }

  ngAfterViewInit(): void {
    this.gms
      .getLocation()
      .pipe(untilDestroyed(this))
      .subscribe({
        next: () => {
          this.hasLoaded.next(true);
          this.gms.fetchGeocodeLatLng();
          if (!this.gms.google) {
            return;
          }

          // let autocomplete = new this.gms.google.maps.places.Autocomplete(
          //   this.addressRef.nativeElement,
          //   this.gms.getAutocompleteOption(),
          // );

          // autocomplete.addListener('place_changed', ($event: google.maps.places.Autocomplete) =>
          //   this.handlePlaceChanged($event),
          // );

          // const subscription: Subscription = this.gms.apiSub$.subscribe(res => {
          //   autocomplete.setValues(res);
          // });
        },
        error: (e: Error) => console.error(e),
      });
  }

  public handleSetTime($event: string): void {
    this.timeEvent.emit($event);
  }

  private handlePlaceChanged(complete: google.maps.places.Autocomplete): void {
    this.ngZone.run(() => {
      //get the place result
      let place: google.maps.places.PlaceResult = complete.getPlace();

      //verify result
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      //set latitude, longitude and zoom
      this.gms.currentLat = place.geometry.location.lat();
      this.gms.currentLng = place.geometry.location.lng();
      this.gms.zoom = 5;
    });
  }
  public handleAddressClick($event: Event): void {
    $event.stopPropagation();
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: AddressComponent,
    };
    this.modal.visibleSub$.next(modalVisible);
  }
}
