import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SLIDES } from 'src/app/core/models/slides.model';

@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
})
export class SlideComponent implements OnInit {
  @Input() slide!: SLIDES;
  @Output() slideEvent = new EventEmitter<any>();
  constructor() {}

  ngOnInit(): void {
    console.log();
  }

  public selectSlideEvent(slide: SLIDES) {
    this.slideEvent.emit(slide);
  }
}
