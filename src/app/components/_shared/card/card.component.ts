import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CardModel } from 'src/app/core/models/cards.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent {
  public isImgComplete: boolean;
  @Input() card!: CardModel;
  constructor(private router: Router) {
    this.isImgComplete = false;
  }

  public handleCardClick(): void {
    if (!this.card || this.card.redirectTo === '') return;
    this.router.navigateByUrl(this.card.redirectTo);
  }

  public handleImgComplete(payload: boolean) {
    this.isImgComplete = payload;
  }
}
