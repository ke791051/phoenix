import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IMGITEMS, SVGITEMS } from 'src/app/core/models/items.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent {
  @Input() svgItem?: SVGITEMS;
  @Input() imgItem?: IMGITEMS;
  @Output() itemEvent = new EventEmitter<any>();
  constructor() {}

  public selectItem(slide: IMGITEMS | SVGITEMS) {
    this.itemEvent.emit(slide);
  }
}
