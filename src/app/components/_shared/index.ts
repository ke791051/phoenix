import { CardComponent } from './card/card.component'
import { ItemComponent } from './item/item.component'
import { SlideComponent } from './slide/slide.component'
import { FormComponent } from './form/form.component'

export const SharedChildComponents = [CardComponent, ItemComponent, SlideComponent, FormComponent]
