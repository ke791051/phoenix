import { Injectable } from '@angular/core'
import { AbstractControl, ValidatorFn } from '@angular/forms'

@Injectable({
  providedIn: 'root',
})
export class FormService {
  formObj = {}
  constructor() {}

  phoneValidate(): ValidatorFn {
    return (control: AbstractControl) => {
      // remove mask
      const value = control.value.replace(/-/g, '').replace(/ /g, '')
      const error = { phoneInValid: true }
      if (value.length !== 9) {
        return error
      }
      return null
    }
  }
}
