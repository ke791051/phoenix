import { FormService } from './form.service';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { setWith } from 'lodash';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Input() set types(value: FORM) {
    this._types = value;
  }
  @Input() title?: string;
  @Input() defaultValue?: string = '';
  @Input() placeholder?: string = '';
  @Input() errorMsg?: string = '格式不正確';
  @Input() required: boolean = true;
  @Output() submitEvent = new EventEmitter<any>();
  public submitText: string;
  public phoneCode: number;
  public confirmation: string;
  public form!: FormGroup;
  public _types: FORM;
  public get types(): FORM {
    if (!this._types) return 'password';
    return this._types;
  }

  constructor(private fb: FormBuilder, private formService: FormService) {
    this.submitText = '驗證手機號碼';
    this.phoneCode = 886;
    this.confirmation =
      '確定即表示您同意Just Kichen可致電或傳送簡訊(包括自動撥號至您提供的號碼。您了解透過簡訊傳送 [STOP] 至89203，即可停止這些訊息。';
    this._types = 'phone';
  }

  ngOnInit(): void {
    this.setDefaultValue();
    if (this.types !== 'phone') {
      setWith(this.formService.formObj, this.types, [this.defaultValue]);
      return;
    }

    setWith(this.formService.formObj, this.types, [
      this.defaultValue,
      [Validators.required, this.formService.phoneValidate()],
    ]);

    this.form = this.fb.group(this.formService.formObj);
  }

  public onSubmit() {
    this.form.markAllAsTouched();
    if (this.form.status === 'INVALID') {
      return;
    }
    if (this.form.value?.phone) {
      const phone = 0 + this.form.value.phone.replace(/-/g, '').replace(/ /g, '');
      this.submitEvent.emit(phone);
    }
  }

  private setDefaultValue(): void {
    this.defaultValue = '';
    this.placeholder = '';
    this.errorMsg = '格式不正確';
    this.required = true;
  }
}

type FORM = 'email' | 'phone' | 'password';
