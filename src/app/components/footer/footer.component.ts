import { CookieService } from 'ngx-cookie-service';
import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Language } from 'src/app/core/enums/language.enum';
import { LanguageCookieKey } from 'src/app/core/constants/language.constant';
@UntilDestroy()
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  @Input() platform!: number;
  public currentLang: '中文' | 'English';
  public langs: string[];
  public ctaButton = [
    { title: 'ios', img: '/assets/svg/app/ios.svg', redirectTo: 'pages/demo' },
    { title: 'android', img: '/assets/svg/app/android.svg', redirectTo: 'pages/demo' },
  ];
  constructor(private cookieService: CookieService, private translocoService: TranslocoService) {
    this.currentLang = '中文';
    this.langs = ['中文', 'English'];
  }

  ngOnInit(): void {
    this.translocoService.langChanges$.pipe(untilDestroyed(this)).subscribe(lang => {
      if (lang === Language.TW) {
        this.currentLang = '中文';
      } else {
        this.currentLang = 'English';
      }
    });
  }

  public handleLangChange(lang: string): void {
    this.cookieService.delete(LanguageCookieKey);
    if (lang === '中文') {
      this.translocoService.setActiveLang(Language.TW);
      this.cookieService.set(LanguageCookieKey, Language.TW);
    } else {
      this.translocoService.setActiveLang(Language.EN);
      this.cookieService.set(LanguageCookieKey, Language.EN);
    }
  }

  selectNav($event: Event) {
    console.log('footer-select', $event);
  }
}
