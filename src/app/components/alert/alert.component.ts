import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent implements OnInit {
  @Input() content!: string;
  @Input() confirmButtonText!: string;
  @Input() isConfirmMode!: boolean;
  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit(): void {}
}
