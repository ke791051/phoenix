import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalOtpComponent } from '../ngbd-modal-otp/ngbd-modal-otp.component';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { get } from 'lodash';

@Component({
  selector: 'app-account-credit',
  templateUrl: './Two-field-modal.component.html',
  styleUrls: ['./Two-field-modal.component.scss'],
})
export class TwoFieldModalComponent implements OnInit {
  @Input() headerTitleUpdate!: string;
  @Input() headerTitleAdd!: string;
  @Input() showClosed = true;
  @Input() buttonTextUpdate!: string;
  @Input() buttonTextDel!: string;
  @Input() buttonTextCancel!: string;
  @Input() buttonTextAdd!: string;
  @Input() labe!: string;
  @Input() placeholder!: string;
  @Input() palceholderValidDate!: string;
  @Input() placeholderCvv!: string;
  @Input() inforContent!: string;
  @Input() formTypeValue!: string;
  public isDisabled: boolean;
  public phoneNumber: string;
  public cardNumber: string;
  public validDate: string;
  public cvv: string;
  public verifyContent: string;
  public formType: string;
  public form_verify = true;

  bodyValidator = Validators.compose([Validators.required, Validators.minLength(10)]);
  form = this.formBuilder.group({
    form_cardNumber: this.formBuilder.control('', {
      validators: Validators.required,
    }),
    form_validDate: this.formBuilder.control('', {
      validators: Validators.required,
    }),
    form_cvv: this.formBuilder.control('', {
      validators: Validators.required,
    }),
    tags: this.formBuilder.array([
      this.formBuilder.control('HTML'),
      this.formBuilder.control('JavaScript'),
      this.formBuilder.control('CSS'),
    ]),
  });

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal, private formBuilder: FormBuilder) {
    console.log('single-filed-modal.component.ts: modalService :>> ', this.modalService);
    this.isDisabled = false;
    this.phoneNumber = '';
    this.cardNumber = '';
    this.validDate = '';
    this.cvv = '';
    this.verifyContent = '';
    this.formType = '';
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(() => {
      if (this.form.valid) {
        console.log(' - - - - debug - - -  valid');
        this.isDisabled = false;
      } else if (this.form.invalid) {
        console.log(' - - - - debug - - -  invalid');
        this.isDisabled = true;
      }
    });
  }

  openOptModal(number: number, verifyvalue: string) {
    const modalRef = this.modalService.open(NgbdModalOtpComponent, { animation: false });
    const input = {
      phoneNumber: number,
      cardNumber: String,
      validDate: String,
      cvv: String,
      buttonText: '確定',
      verifyContent: verifyvalue,
      formType: this.formTypeValue,
    };
    Object.assign(modalRef.componentInstance, input);
  }

  getTagsControl() {
    return this.formArray.controls;
  }

  get formArray() {
    return this.form.get('tags') as FormArray;
  }

  addTag(tag: string) {
    this.formArray.push(this.formBuilder.control(tag));
  }

  removeTag(index: number) {
    this.formArray.removeAt(index);
  }

  send() {
    console.log('this.form.value :>> ', this.form.value);
  }
  submit(): void {
    console.log('submit data to backend');
  }

  get isFormInvalid(): boolean {
    // for submit function
    // const formCardNumber: AbortController = this.form.get('form_cardNumber')
    const form: FormGroup = this.form;
    const formCardNumber: boolean = get(form, 'form_cardNumber.invalid') || false;
    const formValidDate: boolean = get(form, 'form_cardNumber.invalid') || false;
    const formCvv: boolean = get(form, 'form_cardNumber.invalid') || false;
    return formCardNumber && formValidDate && formCvv;
  }
}
