import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ngbd-drop-down',
  templateUrl: './ngbd-drop-down.component.html',
  styleUrls: ['./ngbd-drop-down.component.scss'],
})
export class NgbdDropDownComponent implements OnInit {
  public isInit!: boolean;
  constructor() {}
  ngOnInit(): void {
    this.isInit = true;
  }
}
