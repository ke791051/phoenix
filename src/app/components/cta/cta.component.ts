import { Router } from '@angular/router';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-cta',
  templateUrl: './cta.component.html',
  styleUrls: ['./cta.component.scss'],
})
export class CtaComponent implements OnInit {
  @Input() buttons: BUTTON[] = [];
  @Output() appEvent = new EventEmitter<any>();
  constructor(private router: Router) {}

  ngOnInit(): void {}

  selectApp(app: any) {
    this.appEvent.emit(app);
    this.goPage(app.redirectTo);
  }

  goPage(url: string) {
    if (url) this.router.navigate([url]);
  }
}

interface BUTTON {
  title?: string;
  img: string;
  redirectTo?: string;
}
