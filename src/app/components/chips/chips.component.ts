import { Component, Input, OnInit } from '@angular/core';
import { PopularModel } from '../../core/models/popular.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.scss'],
})
export class ChipsComponent implements OnInit {
  @Input() item?: PopularModel;
  @Input() title?: string;
  @Input() src?: string;
  @Input() redirectTo?: string;

  public hasTitle: boolean;

  constructor() {
    this.hasTitle = false;
  }

  ngOnInit(): void {
    console.log(!!this.title);
    this.hasTitle = !!this.title;
  }
}
