import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  ViewChild,
  NgZone,
  ChangeDetectorRef,
  AfterViewInit,
} from '@angular/core';
import { CartProductModel, ProductModel, Option, OptionItems, PlusItemModel } from '../../core/models/product.model';
import { IModalContentComponent } from '../../core/interface/IModalContentComponent';
import { CartService } from '../../core/services/cart.service';
import { CartProduct } from '@app/core/classes/Product';
import { NgScrollbar } from 'ngx-scrollbar';
import { getNumber } from '../../core/utils/lodash.function';
import { get } from 'lodash';
import { EventMessage } from '../../core/classes/EventMessage';
import { AppService } from '../../core/services/app.service';
import { Subject, throttleTime } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Cart } from '@app/core/classes/Cart';

@UntilDestroy()
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent implements OnInit, AfterViewInit, OnDestroy, IModalContentComponent {
  @Input() product!: ProductModel;
  public cartProduct: CartProductModel;
  public radioSelect: string;
  public checkSelect: number[];
  public count: number;
  public remark: string;
  public remark$: Subject<string>;
  @ViewChild(NgScrollbar)
  scrollbarProductRef!: NgScrollbar;
  constructor(
    private cart: CartService,
    private app: AppService,
    private ngZone: NgZone,
    private modal: NgbActiveModal,
    private ref: ChangeDetectorRef,
  ) {
    this.cartProduct = new CartProduct();
    this.radioSelect = '';
    this.checkSelect = [];
    this.count = 1;
    this.remark = '';
    this.remark$ = new Subject();
    this.ref.detach();
  }

  ngOnInit(): void {
    this.product.options.forEach((item: Option) => {
      if (!item.isMultipleSelect) {
        const defaultOptionIndex: number = getNumber(item, 'default_option', -1);
        this.radioSelect = defaultOptionIndex > -1 ? defaultOptionIndex.toString() : '';
        // const defaultOption: OptionItems =  get(item.option_items, `${defaultOptionIndex}`) || {
        //   product_id: '',
        //   product_name: '',
        //   product_name2: '',
        //   product_pic: '',
        //   product_desc: '',
        //   price: 0,
        //   special_price: 0
        // };
        // this.radioSelect = defaultOptionIndex > -1 ? defaultOption
      }
    });
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
    this.remark$.pipe(throttleTime(2000), untilDestroyed(this)).subscribe((res: string) => {
      this.remark = res;
      this.ref.detectChanges();
    });
  }

  ngOnDestroy(): void {
    this.cartProduct = new CartProduct();
  }

  public checkBoxClickEvent($e: Event, optionIndex: number): void {
    $e.stopPropagation();
    const tmp: number = this.checkSelect.indexOf(optionIndex);
    if (tmp > -1) {
      this.checkSelect.splice(tmp, 1);
    } else {
      this.checkSelect.push(optionIndex);
    }

    this.ref.detectChanges();
  }

  public radioClickEvent($e: Event, optionIndex: number): void {
    $e.stopPropagation();
    this.radioSelect = optionIndex.toString();
    this.ref.detectChanges();
  }

  public handleAddCount(num: number): void {
    this.count += num;
    this.ref.detectChanges();
  }

  public handleRemarkChange($e: Event): void {
    this.remark$.next(($e.target as HTMLInputElement).value);
    // this.remark$.next($event)
  }

  public handleAddToCart(): void {
    let price = 0;
    let plusItemList: PlusItemModel[] = [];
    const radioOptions: Option | undefined = this.product.options.find((item: Option) => !item.isMultipleSelect);
    const checkOptions: Option | undefined = this.product.options.find((item: Option) => item.isMultipleSelect);
    const radioOptionsItem: OptionItems | undefined = get(radioOptions, `option_items.${this.radioSelect}`, undefined);
    const checkOptionsItem: OptionItems[] | undefined = get(checkOptions, 'option_items', undefined);
    if (this.radioSelect !== '' && radioOptionsItem) {
      const res: PlusItemModel = {
        product_id: radioOptionsItem.product_id,
        product: radioOptionsItem,
        price: radioOptionsItem.price,
        count: 1,
        remark: '',
      };
      plusItemList.push(res);
      price += radioOptionsItem.price;
    }
    if (this.checkSelect.length > 0 && checkOptionsItem) {
      const checkSelectList: PlusItemModel[] = this.checkSelect.map((i: number) => {
        const tmp: OptionItems = checkOptionsItem[i];
        const res: PlusItemModel = {
          product_id: tmp.product_id,
          product: tmp,
          price: tmp.price,
          count: 1,
          remark: '',
        };
        price += tmp.price;
        return res;
      });
      plusItemList = plusItemList.concat(checkSelectList);
    }

    const cartProduct: CartProductModel = new Cart({
      product: this.product,
      count: this.count,
      price: price,
      plus_item: plusItemList,
      remark: this.remark,
    });

    this.cart.create(cartProduct).subscribe({
      next: (msg: EventMessage<CartProductModel[]>) => {
        if (msg.status) {
          this.modal.close();
        } else {
          this.app.errorEvent$.next(msg.msg);
        }
      },
      error: err => {},
    });
  }
}
