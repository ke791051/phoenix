import {
  Component,
  Input,
  OnChanges,
  EventEmitter,
  Output,
  ViewChild,
  NgZone,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { BrandProductModel } from 'src/app/core/models/product.model';
import { getString } from '@app/core/utils/lodash.function';
import { NgScrollbar } from 'ngx-scrollbar';
import { AfterViewInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BehaviorSubject, throttleTime } from 'rxjs';
@UntilDestroy()
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnChanges, AfterViewInit {
  @Input() itemList!: BrandProductModel[];
  @Input() spiedTags: string[];
  @Output() sectionChange = new EventEmitter<string>();
  @Output() cardClickEvent = new EventEmitter<string>();
  @ViewChild(NgScrollbar)
  scrollbarRef!: NgScrollbar;

  public currentSection: BehaviorSubject<string>;
  public brandList: string[];
  public isImgComplete: boolean;

  constructor(private ngZone: NgZone, private ref: ChangeDetectorRef) {
    this.currentSection = new BehaviorSubject<string>('menu-scrollspy-0');
    this.brandList = [];
    this.itemList = [];
    this.isImgComplete = false;
    this.spiedTags = [];
    this.ref.detach();
  }

  ngOnChanges(): void {
    this.brandList = this.itemList.map((res: BrandProductModel) => getString(res, 'brandName', ''));
  }

  ngAfterViewInit(): void {
    this.ref.detectChanges();
    this.scrollbarRef.verticalScrolled.pipe(throttleTime(500), untilDestroyed(this)).subscribe(e => {
      let currentSection: string;
      const target: null | EventTarget | null = e.target;
      // const scrollTop = e.target.scrollTop;
      // const parentOffset = e.target.offsetTop;
      // for (let i = 0; i < children.length; i++) {
      //   const element = children[i];
      //   if (this.spiedTags.some(spiedTag => spiedTag === element.tagName)) {
      //     if (element.offsetTop - parentOffset <= scrollTop) {
      //       currentSection = element.id;
      //     }
      //   }
      // }
      // if (currentSection !== this.currentSection) {
      //   this.currentSection = currentSection;
      //   this.sectionChange.emit(this.currentSection);
      // }
    });
  }

  // ngAfterViewInit(): void {
  //   this._scrollSubscription = this.scrollbarRef.scrolled.pipe().subscribe();
  // }
  public trackByFn(index: number): number {
    return index;
  }

  public onSectionChange(newSection: string): void {
    this.currentSection.next(newSection);
  }

  public handleScrollToIndex(index: number): void {
    this.scrollbarRef.scrollToElement(`#menu-scrollspy-${index}`);
  }

  public handleCardClick($event: Event, productId: string): void {
    $event.stopPropagation();
    this.cardClickEvent.emit(productId);
  }
}
