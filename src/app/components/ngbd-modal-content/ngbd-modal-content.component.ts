import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-modal-content',
  templateUrl: './ngbd-modal-content.component.html',
  styleUrls: ['./ngbd-modal-content.component.scss'],
})
export class NgbdModalContentComponent {
  @Input() headerTitle!: string;
  @Input() showClosed!: boolean;
  @Input() buttonText!: string;
  @Input() scrollContent: CONTENT[] = [
    {
      title: '隱私權保護政策說明',
      body: '保護會員的個人隱私是本網站極為重要的經營理念，客戶是我們企業經營中最重要的資產之一。我們所能獲得的所有客戶會員資料，包括姓名身分以及地址email等，都是用來記錄客戶的個人訂單以及配送商品的用途。另外我們為了提供更友善的個人化線上購物服務，會運用客戶投票或其它相關資料來統計分析客戶行為，以做為決策之參考。但是在未經會員同意之下，我們絕不會將您的個人資料洩露至任何本網站以外的團體或其他第三者。另外，我們會根據您過去的購買記錄或瀏覽行為，來推薦您更適合您的商品，儘可能符合您的品味與個人喜好。我們也會分析客戶行為來改善我們的購物平台，使購物更加輕鬆方便。對於電子報訂戶，我們會定期的寄送我們認為對您有價值，有關本網站的重要訊息等等。',
    },
    {
      title: '隱私權保護政策的適用範圍',
      body: '隱私權保護政策內容，包括本網站如何處理在您使用網站服務時收集到的個人識別資料。隱私權保護政策不適用於本網站以外的相關連結網站，也不適用於非本網站所委託或參與管理',
    },
  ];
  public isDisabled: boolean;

  constructor(public activeModal: NgbActiveModal) {
    this.headerTitle = '';
    this.showClosed = true;
    this.buttonText = '';
    this.isDisabled = true;
  }

  toBottom(event: boolean): void {
    if (event === true) {
      this.isDisabled = false;
    }
  }
}

interface CONTENT {
  title: string;
  body: string;
}
