import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-title-item',
  templateUrl: './title-item.component.html',
  styleUrls: ['./title-item.component.scss'],
})
export class TitleItemComponent {
  @Input() title!: string;

  constructor() {}
}
