import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
@UntilDestroy()
@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss'],
})
export class SwitchComponent implements OnChanges {
  @Input() checked!: boolean;
  @Input() txtOn?: string;
  @Input() txtOff?: string;
  @Input() width?: string;
  @Input() height?: string;
  @Output() switchEvent!: EventEmitter<boolean>;

  constructor() {
    this.txtOn = '';
    this.txtOff = '';
    this.checked = false;
    this.switchEvent = new EventEmitter<boolean>();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // changes.this.switchEvent.emit(this.switch);
    console.log(changes);
  }

  public handleSwitchClick($e: Event): void {
    $e.stopPropagation();
    this.switchEvent.emit(!this.checked);
  }
}
