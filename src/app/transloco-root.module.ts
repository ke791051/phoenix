import { HttpClient } from '@angular/common/http';
import {
  TRANSLOCO_LOADER,
  Translation,
  TranslocoLoader,
  TRANSLOCO_CONFIG,
  translocoConfig,
  TranslocoModule,
} from '@ngneat/transloco';
import { Injectable, NgModule } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { EnvService } from 'src/app/core/services/env.service';
import { __deepDotObj, __mergeArrayObj } from 'src/app/core/utils/common.function';
import { LangInfo } from './types/commonType';
import { GoogleSheetApi } from './types/googleSheetApi';
import { LangOrder } from './core/utils/langGoogleMap';
import { getString } from './core/utils/lodash.function';

@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
  constructor(private http: HttpClient, private env: EnvService) {}

  public getTranslateFromApi(lang: string): Observable<any> {
    return this.http
      .get('https://sheets.googleapis.com/v4/spreadsheets/1ybQGUV1095qt3SEUnZ6EOoGoESc-T_gvp0QwAcpf_3U/values/A2:C', {
        params: {
          key: 'AIzaSyDMeK9Qm1PMjCfyRKwYDevLpYywnVZC-_Y',
        },
      })
      .pipe(
        map((res: any) => {
          const arrObj: Array<GoogleSheetApi> = res.values.map((item: Array<string>) => {
            const key: string = getString(item, '0', '');
            let langOrder: string = '-1';
            if (typeof key === 'undefined' || key === null || key === '') {
              return {};
            }

            const langConfig: LangInfo | undefined = LangOrder.find((tmp: LangInfo, index) => {
              if (tmp.name === lang) {
                langOrder = tmp.order;
                return true;
              }
              return false;
            });

            const val: string = getString(item, langOrder, key);
            return { [key]: val };
          });
          const objMerged: any = __mergeArrayObj(arrObj);
          let obj: any;
          try {
            obj = __deepDotObj(objMerged);
          } catch (e) {
            console.error(e);
          }
          console.log('translateApi::', obj);
          return obj;
        }),
      );
  }
  public getTranslation(lang: string): Observable<Translation> {
    if (this.env.translateFromApi) {
      return this.getTranslateFromApi(lang);
    } else {
      return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
    }
  }
}

@NgModule({
  exports: [TranslocoModule],
  providers: [
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        availableLangs: ['en', 'zh-tw'],
        defaultLang: 'zh-tw',
        fallbackLang: 'zh-tw',
        // Remove this option if your application doesn't support changing language in runtime.
        reRenderOnLangChange: true,
        prodMode: environment.production,
      }),
    },
    { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader },
  ],
})
export class TranslocoRootModule {}
