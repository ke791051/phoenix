import { APP_INITIALIZER, Injectable, NgModule, InjectionToken } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LoggerModule } from 'ngx-logger';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './core/shared.module';
import { CoreModule } from './core/core.module';
import { TranslocoRootModule } from './transloco-root.module';
import { LayoutModule } from './layout/layout.module';
import { httpInterceptorProviders } from './core/interceptors';
import { JWTOptions } from './core/classes/AppModuleOption';
import { mapPlaceLoader } from './core/helpers/map.place.initializer';
import { TranslocoService } from '@ngneat/transloco';
import { GoogleMapService } from './core/services/google-map.service';
// import { AgmCoreModule } from '@agm/core';
import { HttpService } from './core/services/http/http.service';
import { httpServiceFactory } from './core/helpers/http.initializer';
import { ReactiveFormsModule } from '@angular/forms';
@Injectable()
export class AppJWTOptions extends JWTOptions {
  constructor() {
    super('n9x5qn8stp68gnvau42qrzmyuvxb4ytr');
  }
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    NgbModule,
    HttpClientModule,
    LoggerModule.forRoot({
      level: environment.logLevel,
      disableConsoleLogging: environment.disableConsole,
      serverLogLevel: environment.serverLogLevel,
      serverLoggingUrl: environment.serverLogUrl,
      colorScheme: ['purple', 'teal', 'green', 'gray', 'yellow', 'red', 'darkred'],
    }),
    CoreModule.forRoot(),
    AppRoutingModule,
    SharedModule,
    LayoutModule,
    TranslocoRootModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: APP_INITIALIZER, useFactory: mapPlaceLoader, multi: true, deps: [TranslocoService, GoogleMapService] },
    {
      provide: HttpService,
      useFactory: httpServiceFactory,
      deps: [HttpClient],
    },
    {
      provide: JWTOptions,
      useClass: AppJWTOptions,
    },
    { provide: NgbActiveModal },
    httpInterceptorProviders,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
