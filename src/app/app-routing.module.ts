import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { AuthGuard } from './core/guard/auth.guard';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignupModule),
  },
  {
    path: 'store',
    component: LayoutComponent,
    loadChildren: () => import('./pages/store/store.module').then(m => m.StoreModule),
  },
  {
    path: 'cart',
    component: LayoutComponent,
    loadChildren: () => import('./pages/cart/cart.module').then(m => m.CartModule),
  },

  {
    path: 'coupon',
    component: LayoutComponent,
    loadChildren: () => import('./pages/coupon/coupon.module').then(m => m.CouponModule),
  },
  {
    path: 'order',
    component: LayoutComponent,
    loadChildren: () => import('./pages/order/order.module').then(m => m.OrderModule),
  },
  {
    path: 'order-info',
    component: LayoutComponent,
    loadChildren: () => import('./pages/order-info/order-info.module').then(m => m.OrderInfoModule),
  },

  {
    path: 'account',
    component: LayoutComponent,
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule),
  },
  {
    path: 'home',
    component: LayoutComponent,
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule),
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      scrollPositionRestoration: 'enabled',
      preloadingStrategy: PreloadAllModules,
      initialNavigation: 'enabledBlocking',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
