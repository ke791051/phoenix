import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/classes/User';
import { IUser } from 'src/app/core/models/user.modal';
import { AuthService } from 'src/app/core/services/auth.service';
import { getString } from 'src/app/core/utils/lodash.function';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  public user: IUser;
  public errorMessage: string;

  constructor(private auth: AuthService) {
    this.user = new User();
    this.errorMessage = '';
    this.auth.apiAuthenticate(this.auth.getToken()).subscribe((state: any) => {
      this.errorMessage = state && state.errorMessage;
    });
  }

  ngOnInit(): void {
    console.log();
  }

  public onSubmit(): void {
    const payload = {
      email: getString(this.user, 'email', ''),
      password: getString(this.user, 'password', ''),
    };
    this.auth.signUp(payload);
  }
}
