import { Component, Input, OnInit } from '@angular/core';
import { AlertComponent } from 'src/app/components/alert/alert.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { dataType } from '../../../../types/data.type';

@Component({
  selector: 'app-account-address',
  templateUrl: './account-address.component.html',
  styleUrls: ['./account-address.component.scss'],
})
export class AccountAddressComponent implements OnInit {
  public placeholder: string;
  public isSettingMode: boolean;
  public isDisabled: boolean;
  public currentHandOver: string;

  @Input() headerTitle!: string;
  @Input() showClosed!: boolean;
  @Input() mode!: string;

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal) {
    console.log('modalService :>> ', this.modalService);
    this.placeholder = '請輸入姓名';
    this.isSettingMode = false;
    this.isDisabled = true;
    this.currentHandOver = '1';
  }

  ngOnInit(): void {
    const result = this.mode;
    if (result === 'Setting') {
      this.isSettingMode = false;
    } else if (result === 'isSetting') {
      this.isSettingMode = true;
    } else {
      this.isSettingMode = true;
    }
  }

  public toggleSettingMode(status: boolean): void {
    this.isSettingMode = status;
  }

  public openCancelModal(): void {
    const modalRef = this.modalService.open(AlertComponent, { animation: false, windowClass: 'logout-modal' });

    const input: dataType = {
      content: '確定要捨棄編輯中的內容嗎?',
      confirmButtonText: '確定',
      isConfirmMode: false,
    };

    Object.assign(modalRef.componentInstance, input);
  }

  public handleHandOver(target: string): void {
    this.currentHandOver = target;
  }
}
