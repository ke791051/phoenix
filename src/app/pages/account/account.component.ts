import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AlertComponent } from 'src/app/components/alert/alert.component';
import { SingleFieldModalComponent } from 'src/app/components/single-field-modal/single-field-modal.component';
import { TwoFieldModalComponent } from 'src/app/components/two-field-modal/two-field-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountAddressComponent } from './components/account-address/account-address.component';
import { ModalService } from '../../core/services/modal.service';
import { ProductComponent } from '@app/components/product/product.component';
import { ModalOptionModel, ModalVisibleModel } from '@app/core/models/modal.model';
import { JsonObject } from '../../types/commonType';
import { Router, ActivatedRoute } from '@angular/router';
import { isString } from 'lodash';
import { UserService } from '../../core/services/user.service';
import { untilDestroyed } from '@ngneat/until-destroy';
import { CustomerModel } from '../../core/models/user.modal';
import { Subject, BehaviorSubject, filter } from 'rxjs';
import { User } from '@app/core/classes/User';
import { StorageService } from '../../core/services/storage.service';
import { StorageType } from '../../core/models/storage.model';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
})
export class AccountComponent implements OnInit {
  public customerNo: string;
  public currentUser?: CustomerModel;
  public currentUser$: BehaviorSubject<CustomerModel>;
  public isMaill: boolean;
  public isApp: boolean;
  constructor(
    private user: UserService,
    private modal: ModalService,
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageService,

    private ref: ChangeDetectorRef,
  ) {
    const modalOpt: ModalOptionModel = {
      modalClass: 'modal-customer',
      backdropClass: 'modal-backdrop-customer',
    };
    this.modal.optionSub$.next(modalOpt);
    this.currentUser$ = new BehaviorSubject<CustomerModel>(new User());
    this.customerNo = '';
    this.isMaill = false;
    this.isApp = false;
    // this.ref.detach();
    console.log('modalService :>> ', this.modal);
  }
  ngOnInit(): void {
    this.setInitData();
  }

  private setInitData(): void {
    const _customerNo: string | null = this.route.snapshot.paramMap.get('id');
    this.customerNo = isString(_customerNo) ? _customerNo : '';
    this.user
      .fetchData(this.customerNo)
      .pipe(filter((res: CustomerModel) => res.customer_no !== ''))
      .subscribe((res: CustomerModel) => {
        this.storage.set(StorageType.local, 'customerInfo', JSON.stringify(res));
        this.currentUser = res;
        // this.ref.detectChanges();
        // this.ref.reattach();
      });
  }
  public openAccountNameModal(): void {
    const prop: JsonObject<string | boolean> = {
      buttonText: '更新',
      placeholder: '請輸入姓名',
      headerTitle: '更新姓名',
      label: '名字',
      showClosed: true,
      _value: 'Dino',
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openAccountPhoneModal(): void {
    const prop: JsonObject<string | boolean> = {
      buttonText: '驗證手機號碼',
      placeholder: '請輸入手機號碼',
      headerTitle: '更新手機號碼',
      label: '手機號碼',
      inforContent: '簡訊驗證碼會傳送至此號碼',
      formTypeValue: 'phone',
      showClosed: true,
      _value: '0905552611',
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openAccountEmailModal(): void {
    const prop: JsonObject<string | boolean> = {
      buttonText: '驗證電子郵件地址',
      placeholder: '請輸入郵件',
      headerTitle: '新增電子郵件地址',
      label: '電子郵件地址',
      inforContent: '簡訊驗證碼會傳送您填寫的電子郵件地址。驗證完成後，訂單發票將會寄送至您的信箱。',
      formTypeValue: 'email',
      showClosed: true,
      _value: 'dinozzo.su@justkitchen.com',
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  //暫時保留
  public openAddressModal(): void {
    const prop: JsonObject<string | boolean> = {
      placeholder: '請輸入您所在的地址',
      headerTitle: '新增常用',
      mode: 'setting',
      showClosed: true,
      _value: '台北市內湖區陽光街365巷39號1樓',
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openModifyAddressModal() {
    const prop: JsonObject<string | boolean> = {
      placeholder: '請輸入您所在的地址',
      headerTitle: '新增常用',
      mode: 'isSetting',
      showClosed: true,
      _value: '台北市內湖區陽光街365巷39號1樓',
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: AccountAddressComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openCompanyAddressModal() {
    const prop: JsonObject<string | boolean> = {
      placeholder: '請輸入公司地址',
      headerTitle: '新增常用',
      mode: 'Setting',
      showClosed: true,
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: AccountAddressComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openLogoutAlert() {
    // const prop: JsonObject<string | boolean> = {
    //   content: '確定要登出？',
    //   confirmButtonText: '確定登出',
    //   isConfirmMode: false,
    //   showClosed: true,
    // };
    // const modalVisible: ModalVisibleModel = {
    //   visible: true,
    //   content: AccountAddressComponent,
    //   instances: prop,
    // };
    // this.modal.visibleSub$.next(modalVisible);
    this.router.navigateByUrl('home');
  }

  public openAccountCreditCardModal() {
    const prop: JsonObject<string | boolean> = {
      buttonTextUpdate: '更新',
      buttonTextDel: '刪除',
      placeholder: '請輸入卡號',
      placeholderCvv: '請輸入安全碼',
      palceholderValidDate: 'MM/YY',
      headerTitleUpdate: '更新付款方式',
      headerTitleAdd: '新增付款方式',
      label: '卡號',
      inforContent: '簡訊驗證碼會傳送至此號碼',
      formTypeValue: 'update',
      showClosed: true,
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openAccountCreditAddCardModal(): void {
    const prop: JsonObject<string | boolean> = {
      buttonTextUpdate: '更新',
      buttonTextDel: '刪除',
      buttonTextCancel: '取消',
      buttonTextAdd: '新增',
      placeholder: '請輸入卡號',
      placeholderCvv: '請輸入安全碼',
      palceholderValidDate: 'MM/YY',
      headerTitleUpdate: '更新付款方式',
      headerTitleAdd: '新增付款方式',
      label: '卡號',
      inforContent: '簡訊驗證碼會傳送至此號碼',
      formTypeValue: 'add',
      showClosed: true,
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: TwoFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public openAccountLanguageModal() {
    const prop: JsonObject<string | boolean> = {
      buttonText: '切換語言',
      placeholder: '請輸入郵件',
      headerTitle: '語言設定',
      label: '語言',
      inforContent: '',
      formTypeValue: 'language',
      showClosed: true,
    };
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: SingleFieldModalComponent,
      instances: prop,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public handleMailRadio($event: boolean) {}
  public handleAppRadio($event: boolean) {}
}
