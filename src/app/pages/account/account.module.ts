import { AccountComponent } from './account.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountRoutingModule } from './account-routing.module';
import { SharedModule } from 'src/app/core/shared.module';
import { SvgIconsModule } from '@ngneat/svg-icon';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountAddressComponent } from './components/account-address/account-address.component';
import { AccountLocationComponent } from './components/account-location/account-location.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SwitchComponent } from '../../components/switch/switch.component';

@NgModule({
  declarations: [AccountComponent, AccountAddressComponent, AccountLocationComponent, SwitchComponent],
  imports: [NgbModule, CommonModule, AccountRoutingModule, SharedModule, SvgIconsModule],
  providers: [NgbActiveModal],
})
export class AccountModule {}
