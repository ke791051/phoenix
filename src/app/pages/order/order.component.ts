import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderResponseModel } from '@app/core/models/order.model';
import { filter, get, isString, isArray } from 'lodash';
import { untilDestroyed, UntilDestroy } from '@ngneat/until-destroy';
import { mockOrderData } from './Mock';
import { OrderResponseDirtyModel } from '../../core/models/order.model';

@UntilDestroy()
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent {
  public currentFilter: number;
  public currentOrder: OrderResponseDirtyModel[] = mockOrderData;
  public filterData: any;
  constructor(private router: Router) {
    this.currentFilter = 0;
    this.filterData = mockOrderData;
  }

  public handleFilterChange(val: number): void {
    if (this.currentFilter !== val) this.currentFilter = val;
    this.filterData = filter(this.currentOrder, (res: any) => {
      res.status === this.currentFilter;
    });
  }

  public getItemInfo(payload: any): string {
    const itemList: any[] = get(payload, 'ItemList', []) || get(payload, 'item_list', []) || [];
    let total: number = 0;
    if (isArray(itemList)) {
      itemList.forEach((item: any) => {
        total += get(item, 'count');
      });
    }

    const shippingWay: string = get(payload, 'shipment_way', 0) === 0 ? '自取' : '外送';

    return `${total}份餐點・${shippingWay}・$${get(payload, 'sub_total_price', 0)}`;
  }

  public handleCardClick($event: Event, item: any): void {
    const no: string = get(item, 'order_no', '');
    this.router.navigateByUrl(`/ord-info`);
  }
  public addTrack(item: any): void {}
}
