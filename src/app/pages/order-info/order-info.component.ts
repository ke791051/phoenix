import { Component, OnInit } from '@angular/core';

import { get, isString } from 'lodash';
import { mockOrderData } from '../order/Mock';

@Component({
  selector: 'app-order-info',
  templateUrl: './order-info.component.html',
  styleUrls: ['./order-info.component.scss'],
})
export class OrderInfoComponent implements OnInit {
  public orderNo: string;
  public list = mockOrderData;
  public currentOrder: any;

  constructor() {
    this.orderNo = '';
  }

  ngOnInit(): void {
    this.setInitData();
    console.log();
  }

  private setInitData(): void {
    // const _orderNo: string | null = this.route.snapshot.paramMap.get('id');
    // this.orderNo = isString(_orderNo) ? _orderNo : '';
    this.currentOrder = this.list.find((item: any) => {
      return get(item, 'order_no', '') === 'O_1647570000';
    });
  }

  public getTimeInfo(payload: string): string {
    // const time: Date = new Date(payload);
    // return `${time.getHours}:${time.getMinutes}`;
    if (!payload) return '';
    const time: string = get(payload.split(' '), '1', '');
    return time;
  }
}
