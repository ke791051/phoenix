import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrderInfoRoutingModule } from './order-info-routing.module';
import { OrderInfoComponent } from './order-info.component';
import { SharedModule } from '../../core/shared.module';

@NgModule({
  declarations: [OrderInfoComponent],
  imports: [CommonModule, OrderInfoRoutingModule, SharedModule],
})
export class OrderInfoModule {}
