import { Component, OnInit } from '@angular/core';
import { GoogleMapService } from '@app/core/services/google-map.service';
import { UserService } from '@app/core/services/user.service';
import { CartService } from '../../core/services/cart.service';
import { StoreService } from '../../core/services/store.service';
import { CartProductModel } from '../../core/models/product.model';
import { ProductComponent } from '@app/components/product/product.component';
import { ModalOptionModel, ModalVisibleModel } from '@app/core/models/modal.model';
import { AddressComponent } from '@app/components/modal/address/address.component';
import { ModalService } from '../../core/services/modal.service';
import { OrderService } from '@app/core/services/order.service';
import { OrderingComponent } from '../../components/modal/ordering/ordering.component';
import { OrderPayload } from '@app/core/classes/Order';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  public status: 'selfPickUp' | 'delivery';
  public isNeedTableware: boolean;
  public taxNumber: string;
  public remark: string;
  public get subPrice(): number {
    let res = 0;
    this.cart.itemList.forEach((item: CartProductModel) => {
      res += item.price;
    });
    return res;
  }
  public get totalPrice(): number {
    let res = 0;
    this.cart.itemList.forEach((item: CartProductModel) => {
      res += item.price;
    });
    return res + this.cart.storeInfo.force_shipping;
  }
  constructor(
    public cart: CartService,
    public gms: GoogleMapService,
    public user: UserService,
    public store: StoreService,
    public modal: ModalService,
    private order: OrderService,
  ) {
    this.status = 'delivery';
    this.user.fetchData('C_1640674416');
    this.store.fetchData(this.gms.currentAddress);
    this.isNeedTableware = false;
    this.taxNumber = '';
    this.remark = '';
  }

  ngOnInit(): void {
    if (this.cart)
      //  this.store.getStoreById()
      console.log();
    const modalOpt: ModalOptionModel = {
      modalClass: 'modal-customer',
      backdropClass: 'modal-backdrop-customer',
    };
    this.modal.optionSub$.next(modalOpt);
  }

  public handleStatusChange(payload: 'selfPickUp' | 'delivery'): void {
    if (payload && payload !== this.status) this.status = payload;
  }

  public handleTaxNumberChange(event: Event): void {
    const target: string = (event.target as HTMLInputElement).value;
    this.taxNumber = target;
  }

  public handleRemarkChange(event: Event): void {
    const target: string = (event.target as HTMLInputElement).value;
    this.remark = target;
  }

  public handleShowPromo(): void {}

  public handleAddressOpen(): void {
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: AddressComponent,
    };
    this.modal.visibleSub$.next(modalVisible);
  }

  public handleSubmit(): void {
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: OrderingComponent,
      instances: { orderInfo: new OrderPayload() },
    };
    this.modal.visibleSub$.next(modalVisible);
  }
}
