import { Observable } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormCanDeactivate } from 'src/app/core/utils/form-can-deactivate';
import { IUser } from 'src/app/core/models/user.modal';
import { User } from 'src/app/core/classes/User';
import { SignInPayLoad } from 'src/app/core/interface/IAuthService';
import { getString } from 'src/app/core/utils/lodash.function';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent extends FormCanDeactivate implements OnInit {
  public user: IUser;
  @ViewChild('form') form!: NgForm;

  public getState: Observable<any>;
  public errorMessage: string | null;

  constructor(private auth: AuthService) {
    super();
    this.user = new User();
    this.errorMessage = '';
    this.getState = new Observable<any>();
  }

  ngOnInit(): void {
    console.log();
  }

  onSubmit(): void {
    const payload: SignInPayLoad = {
      email: getString(this.user, 'email', ''),
      password: getString(this.user, 'password', ''),
    };
    this.auth.logIn(payload);
  }
}
