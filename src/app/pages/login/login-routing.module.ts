import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuard } from 'src/app/core/guard/can-deactivate.guard';
import { LoginComponent } from './login.component';

const routes: Routes = [{ path: '', component: LoginComponent, canDeactivate: [CanDeactivateGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [CanDeactivateGuard],
})
export class LoginRoutingModule {}
