import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  ActivatedRoute,
  Event,
  NavigationExtras,
  NavigationStart,
  ParamMap,
  Params,
  Router,
  RouterEvent,
} from '@angular/router';
import { ProductsService } from '@app/core/services/products.service';
import { StoreService } from '@app/core/services/store.service';
import { isString, groupBy, chain, conforms, map, identity } from 'lodash';
import { ProductModel, BrandProductModel } from '../../core/models/product.model';
import { Subscription, Observable, Subscriber, filter } from 'rxjs';
import { AppService } from '../../core/services/app.service';
import { ProductComponent } from '@app/components/product/product.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Location } from '@angular/common';
import { ModalService } from '../../core/services/modal.service';
import { ModalVisibleModel } from '../../core/models/modal.model';

@UntilDestroy()
@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
})
export class StoreComponent implements OnInit, OnDestroy {
  private storeID: string;
  public isImgComplete: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public store: StoreService,
    public product: ProductsService,
    private app: AppService,
    private modal: ModalService,
  ) {
    this.storeID = '';
    this.isImgComplete = false;
  }

  ngOnInit(): void {
    this.setInitData();
  }

  ngOnDestroy(): void {
    this.product.handleDataClear();
  }

  private handleBrandParser(itemList?: ProductModel[]): BrandProductModel[] {
    if (!itemList) return [];
    const promoItem: BrandProductModel = { brandName: '優惠商品', list: [] };
    const brandList: BrandProductModel[] = map(
      groupBy(itemList, (item: ProductModel) => {
        if (item.special_price > 0) promoItem.list.push(item);
        return item.brand_name;
      }),
      (list: ProductModel[], key: string) => {
        const res: BrandProductModel = {
          brandName: key,
          list,
        };
        return res;
      },
    );

    const res: BrandProductModel[] = [promoItem].concat(brandList);
    return res;
  }

  public handleImgComplete(payload: boolean) {
    this.isImgComplete = payload;
  }

  public handleLocationChange(pid?: string): void {
    const _query: Params = pid ? { queryParams: { pid: pid } } : {};
    const _extra: NavigationExtras = Object.assign({}, { relativeTo: this.route }, _query);
    this.router.navigate([], _extra);
    if (pid) {
      this.checkQueryParam(pid);
    }
  }

  private setInitData(): void {
    const _storeID: string | null = this.route.snapshot.paramMap.get('id');
    this.storeID = isString(_storeID) ? _storeID : '';
    if (_storeID === '') {
      this.router.navigate(['Home']);
    }
    this.store.getStore(this.storeID);
    this.product.fetchData(this.storeID, this.handleBrandParser);
    this.product.dataReadyEvent.pipe(untilDestroyed(this)).subscribe((res: boolean) => {
      if (res) {
        this.checkQueryParam();
      } else {
        console.log('product init error');
      }
    });
    this.modal.visibleSub$
      .pipe(untilDestroyed(this))
      .subscribe((res: ModalVisibleModel) => this.handleLocationChange());
  }

  private checkQueryParam(pid?: string): void {
    const _pid: string | null = pid || this.route.snapshot.queryParamMap.get('pid');
    if (_pid === null) return;
    const target: ProductModel | undefined = this.product.get(_pid);
    if (!target) {
      this.app.errorEvent$.next({
        msg: 'No Data',
        func: () => {
          this.router.navigateByUrl(`/store/${this.storeID}`);
          return;
        },
      });
      return;
    }
    const modalVisible: ModalVisibleModel = {
      visible: true,
      content: ProductComponent,
      instances: { product: target },
    };
    this.modal.visibleSub$.next(modalVisible);
  }
}
