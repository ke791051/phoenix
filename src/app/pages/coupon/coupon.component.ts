import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coupon',
  templateUrl: './coupon.component.html',
  styleUrls: ['./coupon.component.scss'],
})
export class CouponComponent {
  public currentFilter: 'valid' | 'invalid';
  constructor() {
    this.currentFilter = 'valid';
  }

  public handleFilterChange(val: 'valid' | 'invalid'): void {
    if (this.currentFilter !== val) this.currentFilter = val;
  }
}
