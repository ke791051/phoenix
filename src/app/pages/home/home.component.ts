import { GoogleMapService } from 'src/app/core/services/google-map.service';
import { BehaviorSubject, forkJoin, Observable, Subscription, catchError, of } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, Pipe } from '@angular/core';
import { BreakpointsService } from 'src/app/core/services/breakpoints.service';
import { CardService } from 'src/app/core/services/card.service';
import { take, map, switchMap, filter } from 'rxjs/operators';
import { StoreService } from 'src/app/core/services/store.service';
import { StoreModel } from '../../core/models/store.model';
import { get, isString, forEach, groupBy, tap } from 'lodash';
import { CardGroup, CardModel } from 'src/app/core/models/cards.model';
import { Card } from 'src/app/core/classes/Card';
import { EventMessageModel } from 'src/app/core/models/event-message.model';
import { PopularService } from '../../core/services/popular.service';
import { PopularModel } from '../../core/models/popular.model';
import { dataObject } from '../../types/data.type';
import { AppService } from '../../core/services/app.service';
import { imgTextList } from '@app/types/commonType';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy {
  public isPlat$: Observable<number>;
  private cardSub: BehaviorSubject<CardModel[]>;
  public cardSub$: Observable<CardModel[]>;
  private cardGroupSub: BehaviorSubject<CardGroup>;
  public cardGroupSub$: Observable<CardGroup>;
  private popularSub: BehaviorSubject<PopularModel[]>;
  public popularSub$: Observable<PopularModel[]>;
  public dk: number;
  public mb: number;
  public more: boolean;
  public skeletonCount: Array<number>;
  private _subscriber: Subscription;
  public tagList: Array<imgTextList> = [
    {
      src: './assets/svg/hot/item-hot-0.svg',
      title: '優惠',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-1.svg',
      title: '健康餐',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-2.svg',
      title: '日韓',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-3.svg',
      title: '台式',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-4.svg',
      title: '美式',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-5.svg',
      title: '東南亞',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-6.svg',
      title: '炸雞',
      content: '',
      redirectTo: 'pages/demo',
    },
    {
      src: './assets/svg/hot/item-hot-7.svg',
      title: '飲料',
      content: '',
      redirectTo: 'pages/demo',
    },
  ];
  public bannerList: Array<imgTextList> = [
    {
      src: 'assets/images/banner_01.jpg',
      title: 'It"s Good To Eat 新友優惠',
      subtTitle: '美食優惠外送8折',
      content: '活動只到2022/04/01',
      redirectTo: '',
    },
    {
      src: 'assets/images/banner_01.jpg',
      title: 'It"s Good To Eat 新友優惠',
      subtTitle: '美食優惠外送8折',
      content: '活動只到2022/04/01',
      redirectTo: '',
    },
  ];
  constructor(
    private store: StoreService,
    private card: CardService,
    private breakpoints: BreakpointsService,
    private popular: PopularService,
    private gs: GoogleMapService,
    private app: AppService,
  ) {
    this.isPlat$ = this.breakpoints
      .subscribeToLayoutChanges<string>()
      .pipe(map((res: string) => this.breakpoints.setPlatform(res)));
    this.cardSub = new BehaviorSubject<CardModel[]>([]);
    this.cardSub$ = this.cardSub.asObservable();
    this.cardGroupSub = new BehaviorSubject<CardGroup>([]);
    this.cardGroupSub$ = this.cardGroupSub.asObservable();
    this.popularSub = new BehaviorSubject<PopularModel[]>([]);
    this.popularSub$ = this.popularSub.asObservable();
    this.dk = 6;
    this.mb = 4;
    this.more = true;
    this.skeletonCount = new Array<number>(24);
    this._subscriber = Subscription.EMPTY;
  }

  ngOnInit() {
    const card$: Observable<EventMessageModel<CardModel[]>> = this.gs.addressSub$.pipe(
      take(1),
      filter((res: string) => res !== ''),
      switchMap((res: string) => this.store.fetchData(res)),
      filter((list: StoreModel[]) => list.length > 0),
      switchMap((list: StoreModel[]) =>
        this.card.getCard<StoreModel>(list, (item: StoreModel) => this.handleCardFormat(item)),
      ),
      catchError((cardEvent: EventMessageModel<CardModel[]>) => {
        return of(cardEvent);
      }),
    );
    const popular$: Observable<PopularModel[]> = this.popular.fetchData().pipe(
      take(1),
      filter((res: PopularModel[]) => res.length > 0),
    );

    this._subscriber = forkJoin<[EventMessageModel<CardModel[]>, PopularModel[]]>([card$, popular$]).subscribe({
      next: ([cardEvent, popular]) => {
        const card: CardModel[] = get(cardEvent, 'result', []);
        const cardGroup: CardGroup = this.cardGroupParser(card);
        this.cardSub.next(card);
        this.cardGroupSub.next(cardGroup);
        this.popularSub.next(popular);
      },
      error: (err: EventMessageModel<CardModel[]>) => {
        this.app.errorEvent$.next(err.msg);
      },
    });
  }

  ngOnDestroy(): void {
    this._subscriber.unsubscribe();
  }

  public viewMore() {
    this.dk = 0;
    this.mb = 0;
    this.more = false;
  }

  private cardGroupParser(list: CardModel[]): CardGroup {
    const groups: dataObject = groupBy(list, (cards: CardModel) => cards.promotionMode);
    const res: CardGroup = [];
    forEach(groups, (group: CardModel[], key: string) => {
      if (key !== '0') res.push(group);
    });

    return res;
  }

  private handleCardFormat(item: StoreModel): CardModel {
    let promo = '';
    if (item.promotion_mode !== 0) {
      promo = item.promotion_mode === 1 ? `${item.sale_off}% off` : `現折 ${item.discount_amount} 元`;
    }
    const dataMapping: CardModel = new Card(
      item.store_id,
      item.store_pic,
      item.promotion_mode,
      promo,
      item.store_name,
      `${item.distance} m`,
      `${item.order_prepare_time}`,
      `/store/${item.store_id}/product`,
    );

    return dataMapping;
  }

  public trackByFn(index: number): number {
    return index;
  }
}
