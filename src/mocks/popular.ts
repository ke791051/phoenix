import { PopularModel } from '../app/core/models/popular.model';
export const mockPopular: PopularModel[] = [
  {
    brand_id: '00123121',
    brand_name: '熱門1',
    brand_pic: 'assets/svg/brand/item-brand-0.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門1',
    brand_pic: 'assets/svg/brand/item-brand-1.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門2',
    brand_pic: 'assets/svg/brand/item-brand-2.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門3',
    brand_pic: 'assets/svg/brand/item-brand-3.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門4',
    brand_pic: 'assets/svg/brand/item-brand-4.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門5',
    brand_pic: 'assets/svg/brand/item-brand-5.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門6',
    brand_pic: 'assets/svg/brand/item-brand-6.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
  {
    brand_id: '00123121',
    brand_name: '熱門7',
    brand_pic: 'assets/svg/brand/item-brand-7.svg',
    brand_desc: '',
    vendor_id: 'A00001',
    tags: '牛肉麵 水餃',
    popular: 1,
    isPopular: true,
    week: [],
  },
];
